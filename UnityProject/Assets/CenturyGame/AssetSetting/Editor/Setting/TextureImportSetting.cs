﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using System;
using System.IO;
using System.Text;
using System.Text.RegularExpressions;

namespace CenturyGame.AssetSettings
{
	/// <summary>
	/// 贴图导入过滤规则类型，判定路径和命名的规则在这个类型中
	/// </summary>
	[System.Serializable]
	public class TextureImportSetting
	{
		public TextureImportSetting() { }

		public TextureCompressionConfig DefaultCompressionConfig = new TextureCompressionConfig();
		public string whiteList = "DontProcess,Standard Assets,Plugins";
		public List<TextureGroupItem> Groups = new List<TextureGroupItem>();
		public int TextureGroupSelect = 0;
		public bool Enable = true;

		public void AddPath(string path)
		{
			TextureGroupItem item = null;
			string findGroup = null, folder = null;
			byte state = 0;
			int index = 0;
			for (int i = 0; i < Groups.Count; i++)
			{
				item = Groups[i];
				for (int j = 0; j < item.Folders.Count; j++)
				{
					folder = item.Folders[j].Path;
					state = AssetSetting.CheckPathDiff(folder, path);
					if (state > 0)
					{
						findGroup = item.Name;
						index = j + 1;
						break;
					}
				}
				if (findGroup != null)
					break;
			}
			if (state > 0)
			{
				string msg = null;
				switch (state)
				{
					case 1:
						{
							msg = "路径“" + path + "”已经存在于“" + findGroup + "”组第“" + index + "”文件夹。";
							break;
						}
					case 2:
						{
							msg = "路径“" + path + "”包含“" + findGroup + "”组第" + index + "文件夹“" + folder + "”，请重新制定。";
							break;
						}
					case 3:
						{
							msg = "路径“" + path + "”已经被“" + findGroup + "”组第" + index + "文件夹“" + folder + "”包含。";
							break;
						}
					case 4:
						{
							msg = "“" + findGroup + "”组第" + index + "文件夹“" + folder + "”为空。";
							break;
						}
				}
				EditorUtility.DisplayDialog("提示", msg, "确定");
			}
			else
			{
				for (int i = 0; i < Groups.Count; i++)
				{
					item = Groups[i];
					if (item.Select) item.AddPath(path, true);//给当前为选中状态的那个元素添加路径
															  //这里之后需要修改，目前不够灵活,直接传入元素并添加路径，字符串包含判定做成专门的静态方法：返回成功失败 + 弹窗提示
				}
			}
		}

		/// <summary>
		/// 按照路径规则获取相应的配置
		/// </summary>
		public TextureGroupResult GetConfig(string path)
		{
			bool impCfg = false;
			return GetConfig(path, out impCfg);
		}

		/// <summary>
		/// 按照路径规则获取相应的配置
		/// </summary>
		public TextureGroupResult GetConfig(string fullPath, out bool useImportCfg)
		{

			useImportCfg = false;

			if (!Enable || AssetSetting.DisableInLocal)
			{
				return null;
			}

			string[] wList = whiteList.Split(',');

			for (int i = 0; i < wList.Length; i++)
			{
				if (string.IsNullOrWhiteSpace(wList[i]))
					continue;
				else
				{
					if (fullPath.IgnoreCaseContains(wList[i]))//不做处理的关键字，嘘~~~~~~~~
					{
						return null;
					}
				}
			}

			int pointIndex = fullPath.IndexOf(".");//贴图格式后缀“.”之前的字符串个数
			if (pointIndex == -1)
			{
				return null;//如果该路径没有“.”则返回null
			}

			TextureGroupResult textureGroupResult = new TextureGroupResult();

			//优先读入默认的压缩规则，默认规则只定义了压缩格式的内容
			DefaultCompressionConfig.Enable = true;//默认压缩规则保持启用状态
			textureGroupResult.textureCompressionConfig.CopyFrom(DefaultCompressionConfig);

			foreach (TextureGroupItem group in Groups)//过滤处理每个组的规则
			{
				if (group.Enable)
				{
					FolderEditorString folder = null;

					for (int j = 0; j < group.Folders.Count; j++)
					{
						folder = group.Folders[j];
						if (AssetSetting.CheckAssetPath(folder.Path, fullPath))//判断策略组路径
						{
							string fileNameTmp = fullPath.Substring(0, pointIndex);//保存字符串，从开头到“.”之前，即去除后缀

							int lastChar = fileNameTmp.LastIndexOf("/");//从最后一个“/”向前的路径字符数

							string onlyFileName = fileNameTmp.Substring(lastChar + 1);//纯文件名

							string shortPath = fullPath.Substring(folder.Path.Length, lastChar - folder.Path.Length + 1);//文件夹

							if (group.Config.textureImporterConfig.Enable)
							{
								useImportCfg = true;
							}
							textureGroupResult.CopyFrom(group.Config);//匹配入策略组路径规则

							TextureSpecialSuffix textureSpecialConfig = group.GetSpecial(onlyFileName, shortPath);//取出文件名，判断前中后缀
							if (textureSpecialConfig != null)//判断词缀
							{
								if (textureSpecialConfig.textureImporterConfig.Enable)
								{
									useImportCfg = true;
								}
								textureGroupResult.CopyFrom(textureSpecialConfig);//匹配入词缀规则
							}
						}
					}
				}
			}

			//补丁规则：
			if (fullPath.IgnoreCaseContains("2048"))
			{
				textureGroupResult.textureImporterConfig.MaxSize = TextureSize.Max2048;
			}
			else if (fullPath.IgnoreCaseContains("4096"))
			{
				textureGroupResult.textureImporterConfig.MaxSize = TextureSize.Max4096;
			}

			return textureGroupResult;
		}
	}





	/// <summary>
	/// 贴图配置组对象
	/// </summary>
	[System.Serializable]
	public class TextureGroupItem
	{

		public TextureGroupItem(string name)
		{
			Name = name;
		}

		public string Name = null;
		public bool Enable = true;
		public bool Select = false;
		public TextureGroupConfig Config = new TextureGroupConfig();
		public List<FolderEditorString> Folders = new List<FolderEditorString>();
		public List<TextureSpecialSuffix> SpecialSuffixs = new List<TextureSpecialSuffix>();

		public void AddPath(string path, bool save)
		{
			string tmpPath = "";
			bool diff = true;

			for (int i = 0; i < Folders.Count; i++)
			{
				tmpPath = Folders[i].Path;
				int stateID = AssetSetting.CheckPathDiff(tmpPath, path);

				if (stateID == 0)
				{
					diff = true;
				}
				else
				{
					diff = false;
					AssetSetting.stringBuilder.Length = 0;
					AssetSetting.stringBuilder.Append("错误代码：" + stateID + "|");
					AssetSetting.stringBuilder.Append("目录\"");
					AssetSetting.stringBuilder.Append(tmpPath);
					AssetSetting.stringBuilder.Append("\" 与目录\"");
					AssetSetting.stringBuilder.Append(path);
					AssetSetting.stringBuilder.Append("\" 存在包含关系！");
					Debug.LogError(AssetSetting.stringBuilder.ToString());
					break;
				}
			}
			if (diff) Folders.Add(new FolderEditorString(path, save));
		}

		public void AddSuffix()
		{
			TextureSpecialSuffix specialSuffix = new TextureSpecialSuffix();
			SpecialSuffixs.Add(specialSuffix);
		}

		/// <summary>
		/// 获取一个字符串并判断它的前中后缀
		/// </summary>
		public TextureSpecialSuffix GetSpecial(string _fileName, string _path)
		{
			TextureSpecialSuffix result = null;

			for (int i = 0; i < SpecialSuffixs.Count; i++)
			{
				TextureSpecialSuffix tmp = SpecialSuffixs[i];
				if (tmp.Suffix.Length > 0)
				{
					switch (tmp.Match)
					{
						case MatchMod.MatchRight:
							{
								if (_fileName.ToLower().EndsWith(tmp.Suffix.ToLower())) result = tmp;
								break;
							}
						case MatchMod.MatchLeft:
							{
								if (_fileName.ToLower().StartsWith(tmp.Suffix.ToLower())) result = tmp;
								break;
							}
						case MatchMod.MatchContain:
							{
								if (_fileName.IndexOf(tmp.Suffix) > -1) result = tmp;
								break;
							}
						case MatchMod.MatchFolder:
							{
								if (_path.IndexOf(tmp.Suffix) > -1) result = tmp;
								break;
							}
					}
					if (result != null) break;
				}
			}
			return result;
		}

		public void AddPath(string path)
		{
			AddPath(path, true);
		}
	}



	static public class AssetSettingsImporterFun
	{
		/// <summary>
		/// 字符串对比忽略大小写
		/// </summary>
		static public bool IgnoreCaseContains(this string source, string value)
		{
			return (source.IndexOf(value, StringComparison.OrdinalIgnoreCase) >= 0);
		}
	}
}