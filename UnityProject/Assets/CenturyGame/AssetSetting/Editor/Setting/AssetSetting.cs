﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using System.IO;
using System.Text;
using System.Text.RegularExpressions;

namespace CenturyGame.AssetSettings
{
	/// <summary>
	/// 资源设置大类，启动资源管理Tab界面，定义配置文件路径
	/// </summary>
	public sealed class AssetSetting
	{
		public static AssetSettingStore Setting;

		static private string disableInLocalPrefsID = $"AssetSettingDisableInLocal_{PlayerSettings.productName}";
		private static bool disableInLocal;
		public static bool DisableInLocal
		{
			get
			{
				return disableInLocal;
			}

			set
			{
				if (disableInLocal != value)//优化PlayerPrefs读写性能
				{
					PlayerPrefs.SetInt(disableInLocalPrefsID, value ? 1 : 0);
					disableInLocal = value;
				}
			}
		}

		//配置文件保存路径
		public static string currentConfigFilePath { get => AssetSettingManager.Instance.currentConfigFilePath; }

		public static StringBuilder stringBuilder = new StringBuilder();

		[MenuItem("CenturyGame/AssetSetting/资源管理系统")]
		static void AssetImportSetting()
		{
			AssetSettingWindowGUI.ShowWindow();
		}

		public static void ReloadConfig()
		{
			Setting = null;
			GetConfig();
			AssetSettingWindowGUI.SettingGUI = new PageGUIBase(Setting);
		}

		/// <summary>
		/// 读取配置文件
		/// </summary>
		public static void GetConfig()
		{
			GetConfig(currentConfigFilePath);
		}
		/// <summary>
		/// 读取配置文件
		/// </summary>
		public static void GetConfig(string _path)
		{
			disableInLocal = PlayerPrefs.GetInt(disableInLocalPrefsID, 0) == 1;

			if (Setting != null || _path == null)
			{
				//Debug.LogWarning("内存中已有配置信息，不再从Dat文件中加载");
				return;
			}

			if (File.Exists(_path) && _path.EndsWith(".dat"))
			{
				Setting = JsonUtility.FromJson<AssetSettingStore>(System.Text.Encoding.UTF8.GetString(File.ReadAllBytes(_path)));
				Debug.Log("读取设置从\"" + _path + "\"读取成功！");
				for (int i = 0; i < Setting.TextureSetting.Groups.Count; i++)
				{
					TextureGroupItem group = Setting.TextureSetting.Groups[i];
					group.Select = false;
				}
			}
			else
			{
				Debug.LogWarning($"{_path}读取不到配置文件,请先选择或者创建配置文件");
				Setting = new AssetSettingStore();
				Setting.TextureSetting.Enable = false;
				Setting.ModelSetting.Enable = false;
				/*
				TextureGroupItem item = new TextureGroupItem("未命名");
				item.AddPath("Assets/New Folder");//填入默认规则路径
				item.Select = true;
				Setting.TextureSetting.Groups.Add(item);
				Setting.SelectedPage = 0;
				*/
			}
		}


		private static string _datPath = "/CenturyGamePackageRes/AssetSetting/Config/";
		private static string _datName = "ImportSetting.dat";
		private static string PackagesPath = $"{Application.dataPath}{_datPath}{_datName}";

		/// <summary>
		/// 尝试获取标准配置
		/// </summary>
		public static void TryGetStandardConfig()
		{

			Debug.Log(PackagesPath);
			if (File.Exists(PackagesPath))//如有Packages文件夹和ImportSetting.dat文件,则载入配置
			{
				AssetSettingManager.Instance.CurrentConfigFile = AssetDatabase.LoadAssetAtPath("Assets" + _datPath + _datName, typeof(Object));
			}
		}

		/// <summary>
		/// 创建配置文件
		/// </summary>
		public static void CreatConfig()
		{
			TryGetStandardConfig();

			if (!AssetSettingManager.Instance.CurrentConfigFile)
			{
				if (!File.Exists($"{Application.dataPath}{_datPath}"))//如果路径不存在，则创建路径
				{
					Directory.CreateDirectory($"{Application.dataPath}{_datPath}");
					AssetDatabase.Refresh();
				}
				SaveConfig(PackagesPath);
				AssetSettingManager.Instance.CurrentConfigFile = AssetDatabase.LoadAssetAtPath("Assets" + _datPath + _datName, typeof(Object));
			}
		}

		public static void SaveConfig()
		{
			SaveConfig(currentConfigFilePath);
		}

		/// <summary>
		/// 保存Json配置，有结果提示
		/// </summary>
		public static void SaveConfig(string _path)
		{
			if (AssetSetting.Setting == null)
			{
				Debug.Log("当前配置文件为空，生成用于保存的新配置");
				AssetSetting.Setting = new AssetSettingStore();
			}

			if (_path == "")
			{
				Debug.Log("配置文件未指定，无法保存配置！");
				return;
			}

			//下面是重置面板的scroll设置以免dat文件在浏览Editor面板后频繁更新
			int SelectedPageBak = AssetSetting.Setting.SelectedPage;
			AssetSetting.Setting.SelectedPage = 0;
			int TextureGroupSelectBak = AssetSetting.Setting.TextureSetting.TextureGroupSelect;
			AssetSetting.Setting.TextureSetting.TextureGroupSelect = 0;
			List<TextureGroupItem> TexGroupList = AssetSetting.Setting.TextureSetting.Groups;
			for (int i = 0; i < TexGroupList.Count; i++)
			{
				TexGroupList[i].Config.scroll = Vector2.zero;
				TexGroupList[i].Select = i == 0;
			}

			int ModelGroupSelectBak = AssetSetting.Setting.ModelSetting.TextureGroupSelect;
			AssetSetting.Setting.ModelSetting.TextureGroupSelect = 0;
			List<ModelGroupItem> ModelGroupList = AssetSetting.Setting.ModelSetting.Groups;
			for (int i = 0; i < ModelGroupList.Count; i++)
			{
				ModelGroupList[i].Config.scroll = Vector2.zero;
				ModelGroupList[i].Select = i == 0;
			}

			//AssetSetting.Setting.ModelSetting.Enable = true;
			//AssetSetting.Setting.TextureSetting.Enable = true;

			string dat = JsonUtility.ToJson(AssetSetting.Setting, true);
			try
			{
				UnityEngine.Windows.File.WriteAllBytes(_path, System.Text.Encoding.UTF8.GetBytes(dat));
				Debug.Log("保存设置到\"" + _path + "\" 。");
				AssetDatabase.Refresh();
			}
			catch (System.Exception ex)
			{
				Debug.LogError("保存设置到 \"" + _path + "\" 失败，请确认路径是否存在或文件读写权限。" + ex);
			}

			AssetSetting.Setting.SelectedPage = SelectedPageBak;
			AssetSetting.Setting.TextureSetting.TextureGroupSelect = TextureGroupSelectBak;
			AssetSetting.Setting.ModelSetting.TextureGroupSelect = ModelGroupSelectBak;
			for (int i = 0; i < TexGroupList.Count; i++)
			{
				TexGroupList[i].Config.scroll = Vector2.zero;
				TexGroupList[i].Select = i == TextureGroupSelectBak;
			}

			for (int i = 0; i < ModelGroupList.Count; i++)
			{
				ModelGroupList[i].Config.scroll = Vector2.zero;
				ModelGroupList[i].Select = i == ModelGroupSelectBak;
			}
		}



		public static bool CheckAssetPath(string path, string asset)
		{
			if (path.IndexOf(".") > -1) return string.Compare(path, asset) == 0;
			if (path.Length > asset.Length) return false;
			string aPath = asset.Substring(0, path.Length);
			if (string.Compare(path, aPath) == 0)
			{
				aPath = asset.Substring(path.Length);
				return aPath.StartsWith("/");
			}
			return false;
		}


		public static void ImportAssets(string path, System.Type type)
		{
			UnityEngine.Object o = AssetDatabase.LoadAssetAtPath<UnityEditor.DefaultAsset>(path);

			Debug.Log("开始处理路径内的资源..." + type);

			if (type == typeof(Texture2D))
			{
				EditorCoroutineRunner.StartEditorCoroutine(ImportTexture(o));
			}
			else if (type == typeof(GameObject))
			{
				EditorCoroutineRunner.StartEditorCoroutine(ImportMesh(o));
			}
			else
			{
				//Debug.LogWarning("模型类型的导入规则暂未完善");
				//EditorCoroutineRunner.StartEditorCoroutine(ImportMesh(o));
			}
		}

		public static IEnumerator ImportTexture(Object o)
		{
			//TextureProcess textureProcess = new TextureProcess();
			yield return null;
			if (o != null)
			{
				Selection.activeObject = o;
				Object[] assetList = Selection.GetFiltered(typeof(Texture2D), SelectionMode.DeepAssets);
				Selection.objects = assetList;
				EditorApplication.ExecuteMenuItem("Assets/Reimport");
				Selection.objects = new Object[0];
				Debug.Log("贴图资源处理完毕，共处理" + assetList.Length + "份色图");
			}

			/*顺序导入，兼容性较差 停用。
			if (o != null)
			{
				int num = 0;
				Selection.activeObject = o;
				Object[] assetList = Selection.GetFiltered(typeof(Texture2D), SelectionMode.DeepAssets);
				//Selection.objects = assetList;
				Selection.objects = new Object[0];
				int listLength = assetList.Length;
				foreach (Texture2D assetData in assetList)
				{

					string _path = AssetDatabase.GetAssetPath(assetData);
					e = AssetSettingWindowGUI.GUIevent;
					if (e != null && e.keyCode == KeyCode.Escape)
					{
						Debug.Log("任务被人为终止");
						break;
					}
					else
					{
						num++;
						Debug.Log("开始处理贴图(" + num + "/" + listLength + ")：" + _path);
					}
					yield return null;
					TextureImporter textureImporter = AssetImporter.GetAtPath(_path) as TextureImporter;
					textureProcess.OnPreprocessTexture(_path, textureImporter as TextureImporter);
					AssetDatabase.ImportAsset(_path);
				}
				Debug.Log("贴图资源处理完毕，共处理" + num + "份色图");
				
				//EditorApplication.ExecuteMenuItem("Assets/Reimport");
				//Selection.objects = new Object[0];
			}*/
		}

		public static IEnumerator ImportMesh(Object o)
		{
			yield return null;
			if (o != null)
			{
				Selection.activeObject = o;
				Object[] assetList = Selection.GetFiltered(typeof(Object), SelectionMode.DeepAssets);
				List<Object> fbxList = new List<Object>();
				foreach (var obj in assetList)
				{
					string _path = AssetDatabase.GetAssetPath(obj);
					if (_path.ToLower().EndsWith("fbx"))
					{
						fbxList.Add(obj);
					}
				}
				Selection.objects = fbxList.ToArray();
				EditorApplication.ExecuteMenuItem("Assets/Reimport");
				Selection.objects = new Object[0];
				Debug.Log("模型资源处理完毕，共处理" + fbxList.Count + "个模型");
			}
		}




		public static List<string> GetFiles(string path, string[] search, int index = -1)
		{
			List<string> sts = new List<string>();
			int subIndex = 0;
			if (Directory.Exists(path))
			{
				DirectoryInfo direction = new DirectoryInfo(path);
				subIndex = index == -1 ? direction.FullName.Length : index;
				FileInfo[] files = direction.GetFiles("*.*", SearchOption.AllDirectories);
				string tmp = null;

				for (int i = 0; i < files.Length; i++)
				{
					tmp = files[i].FullName;
					if (search == null)
					{
						if (!tmp.EndsWith(".meta"))
						{
							tmp = tmp.Substring(subIndex);
							tmp = tmp.Replace(Path.DirectorySeparatorChar, '/');
							sts.Add(tmp);
						}
					}
					else
					{

						for (int j = 0; j < search.Length; j++)
						{
							if (tmp.ToLower().EndsWith(search[j]))
							{
								tmp = tmp.Substring(subIndex);
								tmp = tmp.Replace(Path.DirectorySeparatorChar, '/');
								sts.Add(tmp);
							}
						}
					}
				}
			}
			return sts;
		}

		/// <summary>
		/// 对比路径并返回对比的结果状态:1，已存在；2，包含之前；3，被之前包含；4，旧字符为空
		/// </summary>
		/// <param name="left">原始值</param>
		/// <param name="right">新值</param>
		/// <returns>1，已存在；2，包含之前；3，被之前包含；4，旧字符为空</returns>
		public static byte CheckPathDiff(string left, string right)
		{
			byte result = 0;

			if (left == "")
			{
				return 4;//4，旧字符为空
			}

			if (string.Compare(left, right) == 0)
			{
				result = 1;//1，已存在
			}
			else
			{
				string[] leftSplits = left.Split(new char[] { '/' });//用路径符号“/”分组
				string[] rightSplits = right.Split(new char[] { '/' });
				int leftSplitCount = leftSplits.Length;
				if (leftSplitCount > rightSplits.Length)
				{
					leftSplitCount = rightSplits.Length;//抛弃左侧超出的长度
				}

				int checkCount = 0;
				for (int i = 0; i < leftSplitCount; i++)
				{
					if (string.Compare(leftSplits[i], rightSplits[i]) != 0)//某个文件夹或者文件名字不同
						break;
					checkCount = i;
				}
				if (checkCount == leftSplitCount - 1)
				{
					if (leftSplits.Length > rightSplits.Length)
						result = 2;//2，包含之前
					else
						result = 3;//3，被之前包含
				}
			}
			return result;
		}

		/// <summary>
		/// 查询CheckPathDiff对比结果的log方法，无实际功能
		/// </summary>
		public static void CheckPathState(int state)
		{

		}


		public static string GetPath(string path)
		{
			int index = path.LastIndexOf("/");
			if (index < 0) return "";
			return path.Substring(0, index);
		}


		/*
	[MenuItem("Assets/资源管理/添加到资源导入设置 #a")]
	static void AddPath()
	{
	UnityEngine.Object o = Selection.activeObject;
	if (o == null) return;
	addPath(o);
	}
	static void addPath(UnityEngine.Object o)
	{
		if (o.GetType() == typeof(UnityEditor.DefaultAsset))
		{
			string path = AssetDatabase.GetAssetPath(o);

			switch (Setting.SelectedPage)
			{
				case 0:
					{
						Setting.TextureSetting.AddPath(path);
						break;
					}
				case 1:
					{
						Setting.ModelSetting.AddPath(path);
						break;
					}
				case 3:
					{
						Setting.OtherSetting.AddPath(path, true, true);
						break;
					}
			}
		}
	}
	*/

	}
}
