﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace CenturyGame.AssetSettings
{
	/// <summary>
	/// 导入配置设置集合
	/// </summary>
	[System.Serializable]
	public class AssetSettingStore
	{
		public int SelectedPage = 0;
		public TextureImportSetting TextureSetting = new TextureImportSetting();
		public OtherImportSetting OtherSetting = new OtherImportSetting();
		public ModelImportSetting ModelSetting = new ModelImportSetting();
	}
}