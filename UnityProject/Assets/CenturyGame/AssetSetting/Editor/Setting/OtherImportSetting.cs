﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace CenturyGame.AssetSettings
{
	[System.Serializable]
	public class OtherImportSetting
	{
		public bool FileNameAllowChinese = false;
		public bool DeleteFileNameHasChinese = false;
		public List<FolderEditorString> WhiteList = new List<FolderEditorString>();

		public void AddPath(string path)
		{
			FolderEditorString item = new FolderEditorString(path, true);
			item.Enable = true;
			WhiteList.Add(item);
		}

		public void AddPath(string path, bool save, bool enable)
		{
			FolderEditorString item = new FolderEditorString(path, save);
			item.Enable = enable;
			WhiteList.Add(item);
		}

		public static bool HasChinese(string input)
		{
			char tmp = ' ';
			if (input != null)
			{

				for (int i = 0; i < input.Length; i++)
				{
					tmp = input[i];
					if (tmp >= 0x4e00 && tmp <= 0x9fbb)
					{
						return true;
					}
				}
			}
			return false;
		}

		private bool checkWhiteList(string asset)
		{
			bool result = false;

			for (int i = 0; i < WhiteList.Count; i++)
			{
				FolderEditorString folder = WhiteList[i];
				if (folder.Enable && AssetSetting.CheckAssetPath(folder.Path, asset))
				{
					result = true;
					break;
				}
			}
			return result;
		}

		public bool Check(string tmp, ref bool delete)
		{
			bool find = false;
			if (!FileNameAllowChinese)
			{
				if (HasChinese(tmp) && !checkWhiteList(tmp))
				{
					if (DeleteFileNameHasChinese)
					{
						delete = true;
						find = true;
					}
					else
					{
						delete = false;
						find = true;
					}
				}
			}
			return find;
		}

		public OtherImportSetting()
		{
		}
	}
}