﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using System.IO;
using System.Text;
using System.Text.RegularExpressions;
using UnityEngine.Experimental.Rendering;

namespace CenturyGame.AssetSettings
{

	[System.Serializable]
	public class ModelImportSetting
	{
		public List<ModelGroupItem> Groups = new List<ModelGroupItem>();
		//public ModelGroupConfig DefaultConfig = new ModelGroupConfig();模型资源不设默认导入模板
		public int TextureGroupSelect = 0;
		public bool Enable = true;

		public void AddPath(string path)
		{
			ModelGroupItem item = null;

			for (int i = 0; i < Groups.Count; i++)
			{
				item = Groups[i];
				if (item.Select) item.AddPath(path, true);
			}
		}

		public ModelImportSetting()
		{
		}

		public ModelConfigResult GetConfig(string fullPath, out bool importCfg, out bool animCfg)
		{
			importCfg = false;
			animCfg = false;

			if (fullPath.IgnoreCaseContains("DontProcess"))//不做处理的关键字，嘘~~~~~~~~
			{
				return null;
			}
			if (!Enable || AssetSetting.DisableInLocal) return null;

			ModelConfigResult modelConfigResult = new ModelConfigResult();
			modelConfigResult.Enable = Enable;
			
			int pointIndex = fullPath.IndexOf(".");
			if (pointIndex == -1) return null;
			ModelGroupItem group = null;

			for (int i = 0; i < Groups.Count; i++)
			{
				group = Groups[i];
				if (group.Enable)
				{
					ModelFolderEditorString folder = null;

					for (int j = 0; j < group.Folders.Count; j++)
					{
						folder = group.Folders[j];
						if (AssetSetting.CheckAssetPath(folder.Path, fullPath))
						{
							string fileNameTmp = fullPath.Substring(0, pointIndex);

							int lastChar = fileNameTmp.LastIndexOf("/", StringComparison.Ordinal);

							modelConfigResult.CheckVertices = folder.CheckVertices;
							modelConfigResult.VerticesCount = folder.VerticesCount;
							modelConfigResult.CheckTriangles = folder.CheckTriangles;
							modelConfigResult.TrianglesCount = folder.TrianglesCount;

							string onlyFileName = fileNameTmp.Substring(lastChar + 1);//纯文件名

							string shortPath = fullPath.Substring(folder.Path.Length, lastChar - folder.Path.Length + 1);//文件夹

							if (group.Config.modelMeshConfig.Enable)
							{
								importCfg = true;
							}
							if (group.Config.modelAnimationConfig.Enable)
							{
								animCfg = true;
							}
							modelConfigResult.CopyFrom(group.Config);

							ModelSpecialSuffix modelSpecialConfig = group.GetSpecial(onlyFileName, shortPath);
							if (modelSpecialConfig != null)
							{
								if (modelSpecialConfig.modelMeshConfig.Enable)
								{
									importCfg = true;
								}
								if (modelSpecialConfig.modelAnimationConfig.Enable)
								{
									animCfg = true;
								}

								modelConfigResult.CopyFrom(modelSpecialConfig);
							}
							return modelConfigResult;
						}
					}
				}
			}

			/*不再提供默认策略
			modelConfigResult.ScaleFactor = DefaultConfig.ScaleFactor;
			modelConfigResult.UseFileScale = DefaultConfig.UseFileScale;
			modelConfigResult.MeshCompression = DefaultConfig.MeshCompression;
			modelConfigResult.RW = DefaultConfig.RW;
			modelConfigResult.OptimizeMesh = DefaultConfig.OptimizeMesh;
			modelConfigResult.ImportBlendShape = DefaultConfig.ImportBlendShape;
			modelConfigResult.GenerateCollider = DefaultConfig.GenerateCollider;
			modelConfigResult.KeepQuad = DefaultConfig.KeepQuad;
			modelConfigResult.IndexFormat = DefaultConfig.IndexFormat;
			modelConfigResult.WeldVertice = DefaultConfig.WeldVertice;
			modelConfigResult.ImportVisibility = DefaultConfig.ImportVisibility;
			modelConfigResult.ImportCamera = DefaultConfig.ImportCamera;
			modelConfigResult.ImportLight = DefaultConfig.ImportLight;
			modelConfigResult.PreserveHierarchy = DefaultConfig.PreserveHierarchy;
			modelConfigResult.SwapUV = DefaultConfig.SwapUV;
			modelConfigResult.GenerateLightmapUV = DefaultConfig.GenerateLightmapUV;
			modelConfigResult.ImportNormal = DefaultConfig.ImportNormal;
			modelConfigResult.NormalMode = DefaultConfig.NormalMode;
			modelConfigResult.SmoothingAngle = DefaultConfig.SmoothingAngle;
			modelConfigResult.Tangent = DefaultConfig.Tangent;
			modelConfigResult.ImportMaterial = DefaultConfig.ImportMaterial;
			modelConfigResult.OptimizeAniScale = DefaultConfig.OptimizeAniScale;
			modelConfigResult.OptimizeAniFloat = DefaultConfig.OptimizeAniFloat;

			modelConfigResult.CheckVertices = false;
			modelConfigResult.VerticesCount = 0;
			modelConfigResult.CheckTriangles = false;
			modelConfigResult.TrianglesCount = 0;
			*/
			return modelConfigResult;
		}
	}

	[System.Serializable]
	public class ModelFolderEditorString : FolderEditorString
	{
		public ModelFolderEditorString(string path, bool save)
		{
			Path = path;
			Save = save;
		}
		public bool CheckVertices = false;
		public int VerticesCount = 0;
		public bool CheckTriangles = false;
		public int TrianglesCount = 0;
	}

	[System.Serializable]
	public class ModelGroupItem
	{

		public ModelGroupItem(string name)
		{
			Name = name;
		}
		public string Name = null;
		public bool Enable = true;
		public bool Select = false;
		public ModelGroupConfig Config = new ModelGroupConfig();
		public List<ModelFolderEditorString> Folders = new List<ModelFolderEditorString>();
		public List<ModelSpecialSuffix> SpecialSuffixs = new List<ModelSpecialSuffix>();

		public void AddPath(string path, bool save)
		{
			string tmpPath = "";
			bool diff = true;

			for (int i = 0; i < Folders.Count; i++)
			{
				tmpPath = Folders[i].Path;
				if (AssetSetting.CheckPathDiff(tmpPath, path) == 0)
				{
					diff = true;
				}
				else
				{
					diff = false;
					AssetSetting.stringBuilder.Length = 0;
					AssetSetting.stringBuilder.Append("目录\"");
					AssetSetting.stringBuilder.Append(tmpPath);
					AssetSetting.stringBuilder.Append("\" 与目录\"");
					AssetSetting.stringBuilder.Append(path);
					AssetSetting.stringBuilder.Append("\" 存在包含关系！");
					Debug.LogError(AssetSetting.stringBuilder.ToString());
					break;
				}
			}
			if (diff) Folders.Add(new ModelFolderEditorString(path, save));
		}
		public void AddSuffix()
		{
			ModelSpecialSuffix specialSuffix = new ModelSpecialSuffix();
			specialSuffix.CopyFrom(Config);
			SpecialSuffixs.Add(specialSuffix);
		}

		public ModelSpecialSuffix GetSpecial(string _fileName, string _path)
		{
			ModelSpecialSuffix result = null;
			for (int i = 0; i < SpecialSuffixs.Count; i++)
			{
				ModelSpecialSuffix tmp = SpecialSuffixs[i];
				if (tmp.Suffix.Length > 0)
				{
					switch (tmp.Match)
					{
						case MatchMod.MatchRight:
							{
								if (_fileName.EndsWith(tmp.Suffix)) result = tmp;
								break;
							}
						case MatchMod.MatchLeft:
							{
								if (_fileName.StartsWith(tmp.Suffix)) result = tmp;
								break;
							}
						case MatchMod.MatchContain:
							{
								if (_fileName.IndexOf(tmp.Suffix, StringComparison.OrdinalIgnoreCase) > -1) result = tmp;
								break;
							}
						case MatchMod.MatchFolder:
							{
								if (_path.IndexOf(tmp.Suffix) > -1) result = tmp;
								break;
							}
					}
				}
			}
			return result;
		}

		public void AddPath(string path)
		{
			AddPath(path, true);
		}
	}
}
