﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace CenturyGame.AssetSettings
{
	/// <summary>
	/// 策略组路径文件夹条目
	/// </summary>
	[System.Serializable]
	public class FolderEditorString
	{

		public FolderEditorString(string path, bool save)
		{
			Path = path;
			Save = save;
		}

		public FolderEditorString()
		{

		}

		public string Path = string.Empty;//路径
		public bool Save = true;
		public bool Select = false;
		public bool Enable = true;
	}
	public enum MatchMod
	{
		MatchLeft = 1,
		MatchContain = 2,
		MatchRight = 3,
		MatchFolder = 4
	}
}