﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
namespace CenturyGame.AssetSettings
{
	/// <summary>
	/// 模型导入设置
	/// </summary>
	[System.Serializable]
	public class ModelMeshConfig
	{
		public bool Enable = false;

		public float ScaleFactor = 1;
		public bool UseFileScale = true;
		public ModelImporterMeshCompression MeshCompression = ModelImporterMeshCompression.Off;
		public bool RW = false;
		public bool OptimizeMesh = true;
		public bool ImportBlendShape = false;
		public bool GenerateCollider = false;
		public bool KeepQuad = false;
		public ModelImporterIndexFormat IndexFormat = ModelImporterIndexFormat.Auto;
		public bool WeldVertice = true;
		public bool ImportVisibility = false;
		public bool ImportCamera = false;
		public bool ImportLight = false;
		public bool PreserveHierarchy = false;
		public bool SwapUV = false;
		public bool GenerateLightmapUV = false;
		public ModelImporterNormals ImportNormal = ModelImporterNormals.Import;
		public ModelImporterNormalCalculationMode NormalMode = ModelImporterNormalCalculationMode.AreaAndAngleWeighted;
		public int SmoothingAngle = 60;
		public ModelImporterTangents Tangent = ModelImporterTangents.CalculateMikk;
		public bool ImportMaterial = false;

		public void CopyFrom(ModelMeshConfig _modelMeshConfig)
		{
			Enable = _modelMeshConfig.Enable;
			if (_modelMeshConfig.Enable)
			{
				ScaleFactor = _modelMeshConfig.ScaleFactor;
				UseFileScale = _modelMeshConfig.UseFileScale;
				MeshCompression = _modelMeshConfig.MeshCompression;
				RW = _modelMeshConfig.RW;
				OptimizeMesh = _modelMeshConfig.OptimizeMesh;
				ImportBlendShape = _modelMeshConfig.ImportBlendShape;
				GenerateCollider = _modelMeshConfig.GenerateCollider;
				KeepQuad = _modelMeshConfig.KeepQuad;
				IndexFormat = _modelMeshConfig.IndexFormat;
				WeldVertice = _modelMeshConfig.WeldVertice;
				ImportVisibility = _modelMeshConfig.ImportVisibility;
				ImportCamera = _modelMeshConfig.ImportCamera;
				ImportLight = _modelMeshConfig.ImportLight;
				PreserveHierarchy = _modelMeshConfig.PreserveHierarchy;
				SwapUV = _modelMeshConfig.SwapUV;
				GenerateLightmapUV = _modelMeshConfig.GenerateLightmapUV;
				ImportNormal = _modelMeshConfig.ImportNormal;
				NormalMode = _modelMeshConfig.NormalMode;
				SmoothingAngle = _modelMeshConfig.SmoothingAngle;
				Tangent = _modelMeshConfig.Tangent;
				ImportMaterial = _modelMeshConfig.ImportMaterial;
			}
		}
	}

	/// <summary>
	/// 未来不用的参数请移至此类
	/// </summary>
	public class ModelImporterDiscardConfig
	{
		public bool Enable = false;
	}

	/// <summary>
	/// 动画参数类
	/// </summary>
	[System.Serializable]
	public class ModelAnimationConfig
	{
		public bool Enable = false;

		public ModelImporterAnimationCompression animationCompression = ModelImporterAnimationCompression.KeyframeReduction;//动画压缩方式
		public float posError = 0.5f;
		public float rotError = 0.5f;
		public float sclError = 0.5f;
		public bool OptimizeAniScale = false;
		public bool OptimizeAniFloat = false;

		public void CopyFrom(ModelAnimationConfig _modelAnimationConfig)
		{
			Enable = _modelAnimationConfig.Enable;

			if (_modelAnimationConfig.Enable)
			{
				animationCompression = _modelAnimationConfig.animationCompression;
				posError = _modelAnimationConfig.posError;
				rotError = _modelAnimationConfig.rotError;
				sclError = _modelAnimationConfig.sclError;
				OptimizeAniScale = _modelAnimationConfig.OptimizeAniScale;
				OptimizeAniFloat = _modelAnimationConfig.OptimizeAniFloat;
			}
		}
	}


	[System.Serializable]
	public class ModelGroupConfig
	{

		public ModelMeshConfig modelMeshConfig = new ModelMeshConfig();
		public ModelAnimationConfig modelAnimationConfig = new ModelAnimationConfig();

		public bool CheckVertices = false;
		public int VerticesCount = 0;
		public bool CheckTriangles = false;
		public int TrianglesCount = 0;

		public Vector2 scroll = Vector2.zero;

		/// <summary>
		/// 从另一个ModelGroupConfig类型获取配置
		/// </summary>
		public void CopyFrom(ModelGroupConfig modelGroupConfig)
		{
			modelMeshConfig.CopyFrom(modelGroupConfig.modelMeshConfig);
			modelAnimationConfig.CopyFrom(modelGroupConfig.modelAnimationConfig);
		}
	}

	public sealed class ModelConfigResult : ModelGroupConfig
	{
		public string AssetPath = null;
		public bool Enable = true;
	}

	[System.Serializable]
	public class ModelSpecialSuffix : ModelGroupConfig
	{
		public string Suffix = string.Empty;

		public ModelSpecialSuffix()
		{

		}
		public bool Select = false;
		public MatchMod Match = MatchMod.MatchRight;

	}
}