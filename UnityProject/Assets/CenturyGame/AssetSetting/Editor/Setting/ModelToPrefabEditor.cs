﻿/*
 *  Author: TurnListen
 * 
 *  Data: 2019-07-17
 * 
 *  Function: Model To Prefab Tool
 * 
 */

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using System.IO;

public class ModelToPrefabEditor : EditorWindow
{
    //模型路径
    static List<GameObject> fbxList = new List<GameObject>();

    //Prefab输出路径
    static string targetPath = "";

    [MenuItem("Assets/资源管理/模型根据LOD拆分为Prefab")]
    private static void InitWindow()
    {
        ModelToPrefabEditor window = (ModelToPrefabEditor)GetWindow(typeof(ModelToPrefabEditor), false, "模型转Prefab");
        GetSelectedPathOrFallback();
    }

    void OnGUI()
    {
        EditorGUILayout.BeginVertical();
        Window();
        EditorGUILayout.EndVertical();
        Repaint();
    }

    void Window()
    {
        GUILayout.Label("模型路径：");
        foreach(var p in fbxList)
        {
            GUILayout.Label(p.name);
        }

        GUILayout.Label("Prefab输出路径：");
        GUILayout.BeginHorizontal();
        EditorGUILayout.TextField(targetPath);
        if (GUILayout.Button("拾取Project窗口文件夹路径"))
        {
            targetPath = AssetDatabase.GUIDToAssetPath(Selection.assetGUIDs[0]);
            string exten = Path.GetExtension(targetPath);
            if(exten != "")
            {
                targetPath = Path.GetDirectoryName(targetPath);
                targetPath = targetPath.Replace("\\","/");
            }
        }
        GUILayout.EndHorizontal();

        GUILayout.Space(100);
        if(GUILayout.Button("导出"))
        {
            ModelToPrefab();
            Debug.Log("导出Prefab");
            this.Close();
        }
    }

    public static void GetSelectedPathOrFallback()
    {
        string path = "Assets";

        //初始化List
        if(fbxList != null)
        {
            fbxList.Clear();
            fbxList = new List<GameObject>();
        }

        var selections = Selection.GetFiltered(typeof(UnityEngine.Object), SelectionMode.Assets);

        foreach (UnityEngine.Object obj in selections)
        {
            path = AssetDatabase.GetAssetPath(obj);

            string exten = Path.GetExtension(path);

            //1、判断是否是Fbx文件，是：操作Fbx，不是：操作该文件夹下所有Fbx
            if (exten == ".FBX" || exten == ".fbx")
            {
                var fbxObj = AssetDatabase.LoadAssetAtPath<GameObject>(path);
                if (!fbxList.Contains(fbxObj))
                {
                    fbxList.Add(fbxObj);
                }
                //Debug.Log("你选中了：模型对象，该对象加入了ObjList列表");
            }
            else if(exten == "")
            {
                string[] guids = AssetDatabase.FindAssets("t: GameObject",new[]{ path });
                for(int i=0; i< guids.Length; i++)
                {
                    var assetPath = AssetDatabase.GUIDToAssetPath(guids[i]);
                    Debug.Log(assetPath);
                    if (assetPath.EndsWith(".FBX") || assetPath.EndsWith(".fbx"))
                    {
                        GameObject gameObj = AssetDatabase.LoadAssetAtPath<GameObject>(assetPath);
                        if (!fbxList.Contains(gameObj))
                        {
                            fbxList.Add(gameObj);
                        }
                    }
                }
                //Debug.Log("你选中了：文件夹，该文件夹下的Fbx加入了ObjList列表");
            }
            else
            {
                Debug.Log("这不是一个可操作对象");
            }
        }
    }

    public static List<T> FindAssetsByType<T>() where T : UnityEngine.Object
    {
        List<T> assets = new List<T>();
        string[] guids = AssetDatabase.FindAssets(string.Format("t:{0}", typeof(T)));
        for (int i = 0; i < guids.Length; i++)
        {
            string assetPath = AssetDatabase.GUIDToAssetPath(guids[i]);
            T asset = AssetDatabase.LoadAssetAtPath<T>(assetPath);
            if (asset != null)
            {
                assets.Add(asset);
            }
        }
        return assets;
    }

    public void ModelToPrefab()
    {
        //1、New Object
        //2、Get Mesh
        //3、Set Material
        //4、Save as Prefab

        EnsureDirectoryExists(targetPath);

        foreach(var fbx in fbxList)
        {
            var fbxinstance = Instantiate(fbx);
            fbxinstance.name = fbx.name;
            var objList = CreateChildGroup(fbxinstance);
            foreach(var obj in objList)
            {
                string destinationPath = targetPath + "/" + obj.name + ".prefab";
                //if (!File.Exists(destinationPath))
                //{
                    //Debug.Log(obj.name);
                    PrefabUtility.SaveAsPrefabAsset(obj, destinationPath);
                    DestroyImmediate(obj);
                //}
                //else
                //{
                    //Debug.Log("已经存在");
                    //PrefabUtility.SaveAsPrefabAssetAndConnect(obj, destinationPath, InteractionMode.AutomatedAction);
                    //DestroyImmediate(obj);
                //}
            }
        }
    }

    private static void EnsureDirectoryExists(string directory)
    {
        if (!Directory.Exists(directory))
            Directory.CreateDirectory(directory);
    }

    private void OnDestroy()
    {
        fbxList.Clear();
        targetPath = "";
        Debug.Log("Window Closed");
    }

    /// <summary>
    /// 拆分模型，重组后生成Prefab
    /// </summary>
    /// <param name="obj"></param>
    /// <returns></returns>
    private List<GameObject> CreateChildGroup(GameObject obj)
    {
        var childs = obj.GetComponentsInChildren<Transform>();

        List<GameObject> Lod_0_List = new List<GameObject>();
        List<GameObject> Lod_1_List = new List<GameObject>();
        List<GameObject> Lod_2_List = new List<GameObject>();
        List<GameObject> Lod_3_List = new List<GameObject>();

        for(int i=0; i<childs.Length; i++)
        {
            Debug.Log(childs[i].name);

            if(childs[i].name.Contains("_LOD1"))
            {
                Lod_1_List.Add(childs[i].gameObject);
            }
            else if (childs[i].name.Contains("_LOD2"))
            {
                Lod_2_List.Add(childs[i].gameObject);
            }
            else if (childs[i].name.Contains("_LOD3"))
            {
                Lod_3_List.Add(childs[i].gameObject);
            }
            else
            {
                Lod_0_List.Add(childs[i].gameObject);
            }
        }

        var lod0Root = CombineLOD(Lod_0_List, obj.name);
        var lod1Root = CombineLOD(Lod_1_List, obj.name + "_LOD1");
        var lod2Root = CombineLOD(Lod_2_List, obj.name + "_LOD2");
        var lod3Root = CombineLOD(Lod_3_List, obj.name + "_LOD3");

        List<GameObject> objlist = new List<GameObject>();
        if(lod0Root != null)
        {
            Debug.Log("objlist: " + objlist.Count);
            objlist.Add(lod0Root);
        }
        if (lod1Root != null)
        {
            Debug.Log("objlist: " + objlist.Count);
            objlist.Add(lod1Root);
        }
        if (lod2Root != null)
        {
            Debug.Log("objlist: " + objlist.Count);
            objlist.Add(lod2Root);
        }
        if (lod3Root != null)
        {
            Debug.Log("objlist: " + objlist.Count);
            objlist.Add(lod3Root);
        }
       
        return objlist;

    }

    private GameObject CombineLOD(List<GameObject> lodLevelList, string rootName)
    {
        if(lodLevelList != null && lodLevelList.Count != 0)
        {
            GameObject lodRoot = new GameObject(rootName);
            foreach (var lodObj in lodLevelList)
            {
                lodObj.transform.parent = lodRoot.transform;
            }
            return lodRoot;
        }
        else
        {
            return null;
        }
    }
}
