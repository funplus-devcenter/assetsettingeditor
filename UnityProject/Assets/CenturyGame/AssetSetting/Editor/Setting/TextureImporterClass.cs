﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

namespace CenturyGame.AssetSettings
{
	public enum TextureSize
	{
		Max32 = 32,
		Max64 = 64,
		Max128 = 128,
		Max256 = 256,
		Max512 = 512,
		Max1024 = 1024,
		Max2048 = 2048,
		Max4096 = 4096
	}

	public enum FoldedRate
	{
		Disable = 0,
		Full = 1,
		Half = 2,
		Quarter = 4
	}

	/// <summary>
	/// 贴图导入参数基类，只包含基本的压缩格式
	/// </summary>
	[System.Serializable]
	public class TextureCompressionConfig
	{
		public TextureCompressionConfig()
		{
			SettingPc.name = "Standalone";
			SettingAndroid.name = "Android";
			SettingIos.name = "iPhone";
		}

		public bool Enable = false;

		public TextureImporterPlatformSettings SettingPc = new TextureImporterPlatformSettings();
		public TextureImporterPlatformSettings SettingAndroid = new TextureImporterPlatformSettings();
		public TextureImporterPlatformSettings SettingIos = new TextureImporterPlatformSettings();

		/// <summary>
		/// 从另一个压缩设置拷贝(如果另一个压缩设置被启用)
		/// </summary>
		public void CopyFrom(TextureCompressionConfig _textureCompressionConfig)
		{
			Enable = _textureCompressionConfig.Enable;
			if (_textureCompressionConfig.Enable)
			{
				_textureCompressionConfig.SettingPc.CopyTo(SettingPc);
				_textureCompressionConfig.SettingAndroid.CopyTo(SettingAndroid);
				_textureCompressionConfig.SettingIos.CopyTo(SettingIos);
			}
		}
	}

	/// <summary>
	/// 贴图导入设置部分的类型对象
	/// </summary>
	[System.Serializable]
	public class TextureImporterConfig
	{
		public bool Enable = true;

		public TextureImporterType TextureType = TextureImporterType.Default;
		public TextureImporterShape TextureShape = TextureImporterShape.Texture2D;
		public TextureImporterNPOTScale NPOTScale = TextureImporterNPOTScale.None;
		public TextureImporterAlphaSource AlphaSource = TextureImporterAlphaSource.FromInput;//注意这个配置会从修改贴图导入设置里漏过去
		public bool AlphaIsTransparency = true;
		public TextureWrapMode WrapMod = TextureWrapMode.Repeat;
		public FilterMode FilterMode = FilterMode.Bilinear;
		public TextureSize MaxSize = TextureSize.Max2048;
		public bool ignoreSmaller = true;//如果目前的贴图设置更小，则不要放大
		public FoldedRate lockFromFileSize = FoldedRate.Disable;//按照贴图实际大小锁定Size比例
										 //public int AnisoLevel = 1;
		public bool RWEnable = false;
		public bool SMipMap = false;
		public bool MipMap = true;
		public bool SRGB = true;
		//public bool G2L = false;
		//public bool LA = false;
		//public bool M5Normal = false;

		/// <summary>
		/// 从Unity贴图原生导入模板获取导入配置,本功能只在EditorUI界面上使用
		/// </summary>
		public void ConfigFromTexTemplate(Texture texTemplate)
		{
			if (texTemplate == null)
			{
				Debug.LogWarning("没有指定贴图模板");
				return;
			}
			TextureImporter importer = (TextureImporter)TextureImporter.GetAtPath(AssetDatabase.GetAssetPath(texTemplate));
			TextureType = importer.textureType;
			TextureShape = importer.textureShape;
			NPOTScale = importer.npotScale;
			AlphaSource = importer.alphaSource;
			AlphaIsTransparency = importer.alphaIsTransparency;
			WrapMod = importer.wrapMode;
			Debug.Log(texTemplate.name + ":WrapMod:" + (int)importer.wrapMode);

			FilterMode = importer.filterMode;
			MaxSize = (TextureSize)importer.maxTextureSize;
			RWEnable = importer.isReadable;
			SMipMap = importer.streamingMipmaps;
			MipMap = importer.mipmapEnabled;
			SRGB = importer.sRGBTexture;
		}

		/// <summary>
		/// Process里主要使用的Copy方法
		/// </summary>
		/// <param name="_textureImporterConfig"></param>
		public void CopyFrom(TextureImporterConfig _textureImporterConfig)
		{
			Enable = _textureImporterConfig.Enable;
			if (_textureImporterConfig.Enable)
			{
				TextureType = _textureImporterConfig.TextureType;
				TextureShape = _textureImporterConfig.TextureShape;
				NPOTScale = _textureImporterConfig.NPOTScale;
				AlphaSource = _textureImporterConfig.AlphaSource;
				AlphaIsTransparency = _textureImporterConfig.AlphaIsTransparency;
				WrapMod = _textureImporterConfig.WrapMod;
				FilterMode = _textureImporterConfig.FilterMode;
				MaxSize = _textureImporterConfig.MaxSize;
				ignoreSmaller = _textureImporterConfig.ignoreSmaller;
				lockFromFileSize = _textureImporterConfig.lockFromFileSize;
				RWEnable = _textureImporterConfig.RWEnable;
				SMipMap = _textureImporterConfig.SMipMap;
				MipMap = _textureImporterConfig.MipMap;
				SRGB = _textureImporterConfig.SRGB;
			}

		}

	}

	/// <summary>
	/// 贴图导入参数拓展类型
	/// </summary>
	[System.Serializable]
	public class TextureGroupConfig
	{
		public Texture texTemplate = null;

		public TextureImporterConfig textureImporterConfig = new TextureImporterConfig();
		public TextureCompressionConfig textureCompressionConfig = new TextureCompressionConfig();

		public Vector2 scroll = Vector2.zero;

		/// <summary>
		/// 从Unity贴图原生导入模板获取配置
		/// </summary>
		public void ConfigFromTexTemplate()
		{
			textureImporterConfig.ConfigFromTexTemplate(texTemplate);
		}

		/// <summary>
		/// 从另一个TextureGroupConfig类型获取配置
		/// </summary>
		public void CopyFrom(TextureGroupConfig _textureGroupConfig)
		{
			textureImporterConfig.CopyFrom(_textureGroupConfig.textureImporterConfig);
			textureCompressionConfig.CopyFrom(_textureGroupConfig.textureCompressionConfig);
		}

		public string ToJson()
		{
			return JsonUtility.ToJson(this);
		}

		public static TextureGroupConfig FromJson(string js)
		{
			return JsonUtility.FromJson<TextureGroupConfig>(js);
		}

	}

	/// <summary>
	/// 继承贴图导入基本设置并添加了一个启用属性
	/// </summary>
	public sealed class TextureGroupResult : TextureGroupConfig
	{
	}

	/// <summary>
	/// 贴图导入参数制作为一个基础类型，给带后缀的资源特别使用？
	/// </summary>
	[System.Serializable]
	public class TextureSpecialSuffix : TextureGroupConfig
	{
		public string Suffix = string.Empty;
		public bool Select = false;
		public MatchMod Match = MatchMod.MatchRight;
	}
}