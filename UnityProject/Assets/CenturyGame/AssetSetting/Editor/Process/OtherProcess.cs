﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

namespace CenturyGame.AssetSettings
{
	public class OtherProcess
	{
		public void ImportedAsset(string asset)
		{
			bool delete = true;
			if (AssetSetting.Setting.OtherSetting.Check(asset, ref delete))
			{
				if (delete)
				{
					AssetSetting.stringBuilder.Length = 0;
					AssetSetting.stringBuilder.Append("资源管理系统发现资源名称包含中文：'");
					AssetSetting.stringBuilder.Append(asset);
					AssetSetting.stringBuilder.Append("',文件已经删除！");
					Debug.LogError(AssetSetting.stringBuilder.ToString());
					AssetDatabase.DeleteAsset(asset);
					AssetDatabase.Refresh();
				}
				else
				{
					AssetSetting.stringBuilder.Length = 0;
					AssetSetting.stringBuilder.Append("资源管理系统发现资源名称包含中文：'");
					AssetSetting.stringBuilder.Append(asset);
					AssetSetting.stringBuilder.Append("'！");
					Debug.LogError(AssetSetting.stringBuilder.ToString());
				}
				EditorApplication.Beep();
			}
		}
	}
}