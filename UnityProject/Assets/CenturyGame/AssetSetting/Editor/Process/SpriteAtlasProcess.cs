﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using UnityEditor.U2D;
using UnityEngine.U2D;

namespace CenturyGame.AssetSettings
{
	public class SpriteAtlasProcess
	{
		private TextureGroupResult textureConfig = null;

		public void SetSpriteAtlasFormPath(string assetPath)
		{
			SpriteAtlas spriteAtlas = (SpriteAtlas)AssetDatabase.LoadAssetAtPath(assetPath, typeof(SpriteAtlas));
			if (!spriteAtlas)
			{
				Debug.LogWarning("spriteAtlas处理失败，path：" + assetPath);
				return;
			}

			AssetSetting.GetConfig();//载入设置
			
			if (AssetSetting.Setting != null)//如果当前有设置
			{
				textureConfig = AssetSetting.Setting.TextureSetting.GetConfig(assetPath);//在这里用路径生成一个TextureGroupResult规则
																										   //返回null是不处理，否则返回一个包含默认压规则的配置，并且捎带一个useImportCfg
			}

			if (textureConfig == null)//如果没有路径对应的配置规则
			{
				//Debug.Log("没有适配规则，不做任何资源导入处理");
				return;
			}

			SetPlantformConfig(spriteAtlas, textureConfig.textureCompressionConfig.SettingPc);
			SetPlantformConfig(spriteAtlas, textureConfig.textureCompressionConfig.SettingAndroid);
			SetPlantformConfig(spriteAtlas, textureConfig.textureCompressionConfig.SettingIos);
		}

		private void SetPlantformConfig(SpriteAtlas spriteAtlas, TextureImporterPlatformSettings source)
		{
			var oriImproter = spriteAtlas.GetPlatformSettings(source.name);

			source.maxTextureSize = oriImproter.maxTextureSize;
			source.compressionQuality = oriImproter.compressionQuality;
			spriteAtlas.SetPlatformSettings(source);
			oriImproter = null;
		}
	}
}