﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

namespace CenturyGame.AssetSettings
{

	/// <summary>
	/// 由Unity调用实现AssetPostprocessor导入处理
	/// </summary>
	public class AssetSettingProcessor : AssetPostprocessor
	{
		private static OtherProcess otherProcess = new OtherProcess();

		#region 图片设置
		private static TextureProcess textureProcess = new TextureProcess();

		private static SpriteAtlasProcess spriteAtlasProcess = new SpriteAtlasProcess();

		public override int GetPostprocessOrder()
		{
			int order = base.GetPostprocessOrder();
			if (!assetPath.Contains("AssetSetting"))
			{
				order += 10;
			}

			//Debug.Log("path:" + assetPath + "\nOldOrder" + base.GetPostprocessOrder() + "\nnewOrder:" + order);
			return order;
		}

		void OnPreprocessAsset()
		{
			if (string.Compare(assetPath, AssetSetting.currentConfigFilePath) == 0)
			{
				Debug.Log($"【AssetSetting】检测到配置文件的更新，重新载入配置：{AssetSetting.currentConfigFilePath}");
				AssetSetting.ReloadConfig();
			}

			if (assetPath.EndsWith(".spriteatlas"))
			{
				spriteAtlasProcess.SetSpriteAtlasFormPath(assetPath);
			}
		}

		void OnPreprocessTexture()
		{
			textureProcess.OnPreprocessTexture(assetPath, assetImporter as TextureImporter);
			//Debug.Log($"处理贴图:{assetPath}");
		}

		void OnPostprocessTexture(Texture2D texture)
		{
			
			//textureProcess.OnPostprocessTexture(texture);暂时停用M5矫正
		}
		#endregion

		/// <summary>
		/// 完成导入
		/// </summary>
		static void OnPostprocessAllAssets(string[] importedAssets, string[] deletedAssets, string[] movedAssets, string[] movedFromAssetPaths)
		{
			if (AssetSetting.Setting != null)
			{
				string tmp = null;

				for (int i = 0; i < importedAssets.Length; i++)
				{
					tmp = importedAssets[i];
					otherProcess.ImportedAsset(tmp);
				}

				for (int i = 0; i < movedAssets.Length; i++)
				{
					tmp = movedAssets[i];
					textureProcess.MoveItem(tmp);
				}
			}
		}

		//#endif
		/*
		static void OnPostprocessAllAssets(string[] importedAssets, string[] deletedAssets, string[] movedAssets, string[] movedFromAssetPaths)//step:4
		{

			foreach (string str in importedAssets)
			{
				Debug.Log("Reimported Asset: " + str);
			}

			foreach (string str in deletedAssets)
			{
				Debug.Log("Deleted Asset: " + str);
			}

			for (int i = 0; i < movedAssets.Length; i++)
			{
				Debug.Log("Moved Asset: " + movedAssets[i] + " from: " + movedFromAssetPaths[i]);
			}
		}
		*/

		#region 模型设置
		private static ModelProcess modelProcess = new ModelProcess();

		void OnPostprocessModel(GameObject g)
		{
			modelProcess.OnPostprocessModel(assetPath, g);
		}

		void OnPreprocessModel()
		{
			modelProcess.OnPreprocessModel(assetPath, assetImporter as ModelImporter);
		}
		#endregion

	}

}