﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using System.Reflection;

namespace CenturyGame.AssetSettings
{
	/// <summary>
	/// 贴图导入的处理逻辑
	/// </summary>
	public class TextureProcess
	{
		private TextureGroupResult textureConfig = null;

		/// <summary>
		/// 导入资源触发，等同OnPreprocessTexture
		/// </summary>
		/// <param name="assetPath">资源路径</param>
		/// <param name="textureImporter">贴图导入过滤器</param>
		public void OnPreprocessTexture(string assetPath, TextureImporter textureImporter)
		{
			//Debug.Log("执行public void OnPreprocessTexture(string assetPath, TextureImporter textureImporter)：" + assetPath);
			//if (onPreprocessAsset) OnPreprocessAsset();//for unity2017
			AssetSetting.GetConfig();//载入设置

			bool useImportCfg = false;//贴图导入配置
			if (AssetSetting.Setting != null)//如果当前有设置
			{
				textureConfig = AssetSetting.Setting.TextureSetting.GetConfig(assetPath, out useImportCfg);//在这里用路径生成一个TextureGroupResult规则
																										   //返回null是不处理，否则返回一个包含默认压规则的配置，并且捎带一个useImportCfg
			}

			if (textureConfig == null)//如果没有路径对应的配置规则
			{
				//Debug.Log("没有适配规则，不做任何资源导入处理");
			}
			else
			{
				if (useImportCfg)//如果接收到的是满配置
				{
					textureImporter.textureType = textureConfig.textureImporterConfig.TextureType;
					textureImporter.textureShape = textureConfig.textureImporterConfig.TextureShape;
					textureImporter.npotScale = textureConfig.textureImporterConfig.NPOTScale;
					textureImporter.wrapMode = textureConfig.textureImporterConfig.WrapMod;
					textureImporter.filterMode = textureConfig.textureImporterConfig.FilterMode;

					if (textureConfig.textureImporterConfig.lockFromFileSize != FoldedRate.Disable)
					{
						object[] parameters = new object[2] { 0, 0 };
						MethodInfo methodInfo = typeof(TextureImporter).GetMethod("GetWidthAndHeight", BindingFlags.NonPublic | BindingFlags.Instance);
						methodInfo.Invoke(textureImporter, parameters);

						int maxSize = Mathf.Max((int)parameters[0], (int)parameters[1]);

						maxSize = Mathf.RoundToInt(maxSize * (1f / (int)textureConfig.textureImporterConfig.lockFromFileSize));

						if (maxSize != 0)
						{
							int[] textureSizes = new int[] {//注意这里的数组应该跟TextureSize枚举类保持一致，必要的话GetValue重建，这里先写死了节约性能
															32,
															64,
															128,
															256,
															512,
															1024,
															2048,
															4096
															};

							maxSize = Mathf.Clamp(maxSize, textureSizes[0], textureSizes[textureSizes.Length - 1]);

							for (int i = 1; i < textureSizes.Length; i++)
							{
								if (maxSize > textureSizes[i]) //如果比当前Size大，则到下个Size对比
								{
									continue;
								}
								else
								{
									float t = Mathf.InverseLerp(textureSizes[i - 1], textureSizes[i], maxSize);
									int v = t > 0.7f ? i : i - 1;//确定内插接近值
									maxSize = textureSizes[v];
									break;
								}
							}
						}

						//Debug.Log("maxSize：" + maxSize + "|size:" + parameters[0] + "," + parameters[1]);
						textureImporter.maxTextureSize = maxSize;
					}

					if (!textureConfig.textureImporterConfig.ignoreSmaller || textureImporter.maxTextureSize > (int)textureConfig.textureImporterConfig.MaxSize)
					{
						textureImporter.maxTextureSize = (int)textureConfig.textureImporterConfig.MaxSize;
					}

					/*
					if (textureImporter.anisoLevel == 0)
					{
						textureImporter.anisoLevel = 1;//纠正之前的设置错误，anisoLevel不得低于默认值1
					}
					*/

					//textureImporter.anisoLevel = textureConfig.textureImporterConfig.AnisoLevel;
					textureImporter.isReadable = textureConfig.textureImporterConfig.RWEnable;
					textureImporter.sRGBTexture = textureConfig.textureImporterConfig.SRGB;
					textureImporter.streamingMipmaps = textureConfig.textureImporterConfig.SMipMap;
					textureImporter.mipmapEnabled = textureConfig.textureImporterConfig.MipMap;
				}
				setAlpha(textureImporter, textureConfig.textureCompressionConfig.SettingPc);
				setAlpha(textureImporter, textureConfig.textureCompressionConfig.SettingAndroid);
				setAlpha(textureImporter, textureConfig.textureCompressionConfig.SettingIos);
			}
		}


		/// <summary>
		/// 透明与不透明压缩格式的对应
		/// </summary>

		private void setAlpha(TextureImporter textureImporter, TextureImporterPlatformSettings source)
		{
			var oriImproter = textureImporter.GetPlatformTextureSettings(source.name);

			source.maxTextureSize = textureImporter.maxTextureSize;
			source.compressionQuality = oriImproter.compressionQuality;
			TextureImporterFormat tif = source.format;//缓存原格式

			if (textureImporter.DoesSourceTextureHaveAlpha())//如果有Alpha，则保持有A的压缩格式
			{
				textureImporter.alphaSource = textureConfig.textureImporterConfig.AlphaSource;
				//textureImporter.alphaIsTransparency = (textureImporter.alphaSource != TextureImporterAlphaSource.None);
				//textureImporter.alphaIsTransparency = textureConfig.textureImporterConfig.AlphaIsTransparency;
				
#if UNITY_2019_1_OR_NEWER
				//如果是新版本,所有带A的ASTC格式全部处理为不带A，Unity2019之后会自动处理
				switch (source.format)
				{
					case TextureImporterFormat.ASTC_RGBA_4x4:
						{
							source.format = TextureImporterFormat.ASTC_RGB_4x4;
							break;
						}
					case TextureImporterFormat.ASTC_RGBA_5x5:
						{
							source.format = TextureImporterFormat.ASTC_RGB_5x5;
							break;
						}
					case TextureImporterFormat.ASTC_RGBA_6x6:
						{
							source.format = TextureImporterFormat.ASTC_RGB_6x6;
							break;
						}
					case TextureImporterFormat.ASTC_RGBA_8x8:
						{
							source.format = TextureImporterFormat.ASTC_RGB_8x8;
							break;
						}
					case TextureImporterFormat.ASTC_RGBA_10x10:
						{
							source.format = TextureImporterFormat.ASTC_RGB_10x10;
							break;
						}
					case TextureImporterFormat.ASTC_RGBA_12x12:
						{
							source.format = TextureImporterFormat.ASTC_RGB_12x12;
							break;
						}
				}
#endif
			}
			else//如果没有alpha，则选择对应的不带A通道
			{
				textureImporter.alphaSource = TextureImporterAlphaSource.None;

				switch (source.format)
				{
					case TextureImporterFormat.DXT5:
						{
							source.format = TextureImporterFormat.DXT1;
							break;
						}
					case TextureImporterFormat.ETC2_RGBA8:
						{
							source.format = TextureImporterFormat.ETC2_RGB4;
							break;
						}
					case TextureImporterFormat.ETC2_RGBA8Crunched:
						{
							source.format = TextureImporterFormat.ETC_RGB4Crunched;
							break;
						}
					case TextureImporterFormat.ASTC_RGBA_4x4:
						{
							source.format = TextureImporterFormat.ASTC_RGB_4x4;
							break;
						}
					case TextureImporterFormat.ASTC_RGBA_5x5:
						{
							source.format = TextureImporterFormat.ASTC_RGB_5x5;
							break;
						}
					case TextureImporterFormat.ASTC_RGBA_6x6:
						{
							source.format = TextureImporterFormat.ASTC_RGB_6x6;
							break;
						}
					case TextureImporterFormat.ASTC_RGBA_8x8:
						{
							source.format = TextureImporterFormat.ASTC_RGB_8x8;
							break;
						}
					case TextureImporterFormat.ASTC_RGBA_10x10:
						{
							source.format = TextureImporterFormat.ASTC_RGB_10x10;
							break;
						}
					case TextureImporterFormat.ASTC_RGBA_12x12:
						{
							source.format = TextureImporterFormat.ASTC_RGB_12x12;
							break;
						}
					case TextureImporterFormat.PVRTC_RGBA4:
						{
							source.format = TextureImporterFormat.PVRTC_RGB4;
							break;
						}
				}
			}
			//Debug.LogWarning("source.format:" + source.format + "(" + (int)source.format + ")");
			textureImporter.SetPlatformTextureSettings(source);
			source.format = tif;//还原预保存的格式
			oriImproter = null;
		}




		public void MoveItem(string path)
		{
			TextureGroupResult tConfig = AssetSetting.Setting.TextureSetting.GetConfig(path);
			if (tConfig != null)
			{
				AssetDatabase.ImportAsset(path, ImportAssetOptions.ForceUpdate);
			}
		}

		/*
		/// <summary>
		/// M5GammaToLine矫正和M5法线矫正（R=>RGB,G=>A）
		/// </summary>
		/// <param name="texture"></param>
		public void OnPostprocessTexture(Texture2D texture)
		{
			Debug.Log("执行public void OnPostprocessTexture(Texture2D texture)：" + texture.name);
			if (textureConfig != null)
			{
				if (textureConfig.G2L)
				{
					if (textureConfig.LA)
					{

						for (int m = 0; m < texture.mipmapCount; m++)
						{
							Color[] c = texture.GetPixels(m);

							for (int i = 0; i < c.Length; i++)
							{
								c[i] = Gamma2LinearAlpha(c[i]);
							}
							texture.SetPixels(c, m);
							//covertToLinearTexture(texture, m);
						}
						texture.Apply(true);
					}
					else
					{

						for (int m = 0; m < texture.mipmapCount; m++)
						{
							Color[] c = texture.GetPixels(m);

							for (int i = 0; i < c.Length; i++)
							{
								c[i] = Gamma2Linear(c[i]);
							}
							texture.SetPixels(c, m);
						}
						texture.Apply(true);
					}
				}
				if (textureConfig.M5Normal)
				{

					for (int m = 0; m < texture.mipmapCount; m++)
					{
						Color[] c = texture.GetPixels(m);
						Color color = Color.red;

						for (int i = 0; i < c.Length; i++)
						{
							color = c[i];
							color.a = color.r;
							color.r = color.b = color.g;
							c[i] = color;
						}
						texture.SetPixels(c, m);
					}
					texture.Apply(true);
				}
				textureConfig = null;
			}
		}
		*/
		//以下全是各种算法工具Func----------------------------------------------------------------------------------------

		void covertToLinearTexture(Texture2D source, int mip)
		{
			int texw = source.width / (mip + 1);
			int texh = source.height / (mip + 1);
			var pixels = source.GetPixels(mip);
			var offs = 0;
			if (texw * texh == pixels.Length)
			{
				//var k1Per15 = 0.00392157f;
				var k1Per16 = 0.00390626f;
				var k3Per16 = 0.1875f;
				var k5Per16 = 0.3125f;
				var k7Per16 = 0.4375f;
				//var max = 256.0f;

				for (var y = 0; y < texh; y++)
				{

					for (var x = 0; x < texw; x++)
					{
						//Color pix = Gamma2LinearAlpha(pixels[offs]);
						//float a = pix.a;
						//var a2 = Mathf.Clamp01(Mathf.Floor(a * max) * k1Per15);
						//var ae = pix.a - a2;
						//pix.a = a2;
						//pixels[offs] = pix;
						Color pix = pixels[offs];
						Color tmp = Color.black;
						tmp.r = GammaToLinear(pix.r);
						tmp.g = GammaToLinear(pix.g);
						tmp.b = GammaToLinear(pix.b);
						tmp.a = LinearToGamma(pix.a);
						var ae = pix.a - tmp.a;
						pixels[offs] = tmp;
						var n1 = offs + 1;
						var n2 = offs + texw - 1;
						var n3 = offs + texw;
						var n4 = offs + texw + 1;
						if (x < texw - 1)
						{
							pixels[n1].a += ae * k7Per16;
						}
						if (y < texh - 1)
						{
							pixels[n3].a += ae * k5Per16;
							if (x > 0)
							{
								pixels[n2].a += ae * k3Per16;
							}
							if (x < texw - 1)
							{
								pixels[n4].a += ae * k1Per16;
							}
						}
						offs++;
					}
				}
			}
			else
			{

				for (int i = 0; i < pixels.Length; i++)
				{
					//不符合2次幂的图，alpha 强制Linear修复，不做抖动优化
					pixels[i] = Gamma2LinearAlpha(pixels[i]);
				}
			}
			source.SetPixels(pixels, mip);
		}


		//covert to 4444

		//void OnPostprocessTexture(Texture2D texture)
		//{

		//    if (!Fit(assetPath))
		//    {
		//        return;
		//    }
		//    var texw = texture.width;
		//    var texh = texture.height;
		//    var pixels = texture.GetPixels();
		//    var offs = 0;
		//    var k1Per15 = 1.0f / 15.0f;
		//    var k1Per16 = 1.0f / 16.0f;
		//    var k3Per16 = 3.0f / 16.0f;
		//    var k5Per16 = 5.0f / 16.0f;
		//    var k7Per16 = 7.0f / 16.0f;

		//    for (var y = 0; y < texh; y++)
		//    {

		//        for (var x = 0; x < texw; x++)
		//        {
		//            float a = pixels[offs].a;
		//            float r = pixels[offs].r;
		//            float g = pixels[offs].g;
		//            float b = pixels[offs].b;
		//            var a2 = Mathf.Clamp01(Mathf.Floor(a * 16) * k1Per15);
		//            var r2 = Mathf.Clamp01(Mathf.Floor(r * 16) * k1Per15);
		//            var g2 = Mathf.Clamp01(Mathf.Floor(g * 16) * k1Per15);
		//            var b2 = Mathf.Clamp01(Mathf.Floor(b * 16) * k1Per15);
		//            var ae = a - a2;
		//            var re = r - r2;
		//            var ge = g - g2;
		//            var be = b - b2;
		//            pixels[offs].a = a2;
		//            pixels[offs].r = r2;
		//            pixels[offs].g = g2;
		//            pixels[offs].b = b2;

		//var n1 = offs + 1;   // (x+1,y)

		//var n2 = offs + texw - 1; // (x-1 , y+1)

		//var n3 = offs + texw;  // (x, y+1)

		//var n4 = offs + texw + 1; // (x+1 , y+1)

		//            if (x < texw - 1)
		//            {
		//                pixels[n1].a += ae * k7Per16;
		//                pixels[n1].r += re * k7Per16;
		//                pixels[n1].g += ge * k7Per16;
		//                pixels[n1].b += be * k7Per16;
		//            }

		//            if (y < texh - 1)
		//            {
		//                pixels[n3].a += ae * k5Per16;
		//                pixels[n3].r += re * k5Per16;
		//                pixels[n3].g += ge * k5Per16;
		//                pixels[n3].b += be * k5Per16;

		//                if (x > 0)
		//                {
		//                    pixels[n2].a += ae * k3Per16;
		//                    pixels[n2].r += re * k3Per16;
		//                    pixels[n2].g += ge * k3Per16;
		//                    pixels[n2].b += be * k3Per16;
		//                }

		//                if (x < texw - 1)
		//                {
		//                    pixels[n4].a += ae * k1Per16;
		//                    pixels[n4].r += re * k1Per16;
		//                    pixels[n4].g += ge * k1Per16;
		//                    pixels[n4].b += be * k1Per16;
		//                }
		//            }
		//            offs++;
		//        }
		//    }
		//    texture.SetPixels(pixels);
		//    EditorUtility.CompressTexture(texture, TextureFormat.RGBA4444, TextureCompressionQuality.Best);
		//}

		private static Color Gamma2Linear(Color c)
		{
			Color result = c;
			result.r = GammaToLinear(c.r);
			result.g = GammaToLinear(c.g);
			result.b = GammaToLinear(c.b);
			return result;
		}

		private static Color Gamma2LinearAlpha(Color c)
		{
			Color result = c;
			result.r = GammaToLinear(c.r);
			result.g = GammaToLinear(c.g);
			result.b = GammaToLinear(c.b);
			//if (c.a < 1) result.a = GammaToLinear(c.a);
			if (c.a < 1) result.a = LinearToGamma(c.a);
			return result;
		}

		static float GammaToLinear(float c)
		{
			if (c <= 0.04045f) return c / 12.92f;
			else if (c < 1.0f) return Mathf.Pow((c + 0.055f) / 1.055f, 2.4f);
			else return Mathf.Pow(c, 2.2f);
		}

		static float LinearToGamma(float c)
		{
			if (c <= 0.0f) return 0.0f;
			else if (c <= 0.0031308f) return c * 12.92f;
			else if (c < 1.0f) return 1.055f * Mathf.Pow(c, 0.4166667f) - 0.055f;
			else return Mathf.Pow(c, 0.45454545f);
		}
	}
}
