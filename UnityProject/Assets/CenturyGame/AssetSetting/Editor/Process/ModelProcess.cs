﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

namespace CenturyGame.AssetSettings
{
	public class ModelProcess
	{
		private ModelConfigResult modelConfig = null;

		public void OnPreprocessModel(string assetPath, ModelImporter modelImporter)
		{
			bool importCfg = false;//是否更新了导入配置
			bool animCfg = false;//是否更新了动画配置
								 //if (onPreprocessAsset) OnPreprocessAsset();//for unity2017
			AssetSetting.GetConfig();
			if (AssetSetting.Setting != null)
			{
				modelConfig = AssetSetting.Setting.ModelSetting.GetConfig(assetPath, out importCfg, out animCfg);
			}
			if (modelConfig != null)
			{
				modelConfig.AssetPath = assetPath;
				if (importCfg)
				{
					ModelMeshConfig miCfg = modelConfig.modelMeshConfig;
					modelImporter.globalScale = miCfg.ScaleFactor;
					modelImporter.useFileScale = miCfg.UseFileScale;
					modelImporter.meshCompression = miCfg.MeshCompression;
					modelImporter.isReadable = miCfg.RW;
					modelImporter.optimizeMesh = miCfg.OptimizeMesh;
					modelImporter.importBlendShapes = miCfg.ImportBlendShape;
					modelImporter.addCollider = miCfg.GenerateCollider;
					modelImporter.keepQuads = miCfg.KeepQuad;
					modelImporter.indexFormat = miCfg.IndexFormat;
					modelImporter.weldVertices = miCfg.WeldVertice;
					modelImporter.importVisibility = miCfg.ImportVisibility;
					modelImporter.importCameras = miCfg.ImportCamera;
					modelImporter.importLights = miCfg.ImportLight;
					modelImporter.preserveHierarchy = miCfg.PreserveHierarchy;
					modelImporter.swapUVChannels = miCfg.SwapUV;
					//modelImporter.generateSecondaryUV = miCfg.GenerateLightmapUV;
					modelImporter.importNormals = miCfg.ImportNormal;
					modelImporter.normalCalculationMode = miCfg.NormalMode;
					modelImporter.normalSmoothingAngle = miCfg.SmoothingAngle;
					modelImporter.importTangents = miCfg.Tangent;
					modelImporter.importMaterials = miCfg.ImportMaterial;
				}
				if (animCfg)
				{
					ModelAnimationConfig maCfg = modelConfig.modelAnimationConfig;
					modelImporter.animationCompression = maCfg.animationCompression;
					modelImporter.animationPositionError = maCfg.posError;
					modelImporter.animationRotationError = maCfg.rotError;
					modelImporter.animationScaleError = maCfg.sclError;
				}
			}
		}

		public void OnPostprocessModel(string assetPath, GameObject g)
		{
			if (modelConfig != null)
			{
				if (modelConfig.CheckVertices || modelConfig.CheckTriangles || !modelConfig.modelMeshConfig.ImportMaterial)
				{
					bool result = true;
					checkMesh(g.transform, ref result);
					if (!result)
					{
						deleteFile(modelConfig.AssetPath);
						EditorApplication.Beep();
					}
				}

				if (modelConfig.modelAnimationConfig.Enable && (modelConfig.modelAnimationConfig.OptimizeAniFloat || modelConfig.modelAnimationConfig.OptimizeAniScale))
				{
					optimizeAni(assetPath, g);
				}
			}
			modelConfig = null;
		}

		private readonly string optimizeStr = "f3";
		/// <summary>
		/// 优化动画float精度
		/// </summary>
		private void optimizeAni(string assetPath, GameObject g)
		{
			List<AnimationClip> animationClipList = new List<AnimationClip>(AnimationUtility.GetAnimationClips(g));
			if (animationClipList.Count == 0)
			{
				AnimationClip[] objectList = UnityEngine.Object.FindObjectsOfType(typeof(AnimationClip)) as AnimationClip[];
				animationClipList.AddRange(objectList);
			}
			foreach (AnimationClip theAnimation in animationClipList)
			{
				if (theAnimation.name.Contains("__preview__"))
					continue;
				try
				{
					foreach (EditorCurveBinding theCurveBinding in AnimationUtility.GetCurveBindings(theAnimation))
					{
						string name = theCurveBinding.propertyName.ToLower();
						
						if (modelConfig.modelAnimationConfig.OptimizeAniScale && name.Contains("scale"))
						{
							AnimationUtility.SetEditorCurve(theAnimation, theCurveBinding, null);
							continue;
						}

						if (modelConfig.modelAnimationConfig.OptimizeAniFloat)
						{
							AnimationCurve curves = AnimationUtility.GetEditorCurve(theAnimation, theCurveBinding);
							Keyframe key;
							Keyframe[] keyFrames;
							if (curves == null || curves.keys == null)
							{
								continue;
							}
							keyFrames = curves.keys;
							for (int i = 0; i < keyFrames.Length; i++)
							{
								key = keyFrames[i];
								key.value = float.Parse(key.value.ToString(optimizeStr));
								key.inTangent = float.Parse(key.inTangent.ToString(optimizeStr));
								key.outTangent = float.Parse(key.outTangent.ToString(optimizeStr));
								keyFrames[i] = key;
							}
							curves.keys = keyFrames;
							theAnimation.SetCurve(theCurveBinding.path, theCurveBinding.type, theCurveBinding.propertyName, curves);
						}
					}
				}
				catch (System.Exception e)
				{
					Debug.LogError(string.Format("CompressAnimationClip Failed !!! animationPath : {0} error: {1}", assetPath, e));
				}
			}
		}

		private void checkMesh(Transform t, ref bool go)
		{
			Mesh mesh = null;
			MeshFilter meshFilter = t.GetComponent<MeshFilter>();
			if (meshFilter == null)
			{
				SkinnedMeshRenderer skinnedMeshRenderer = t.GetComponent<SkinnedMeshRenderer>();
				if (skinnedMeshRenderer != null) mesh = skinnedMeshRenderer.sharedMesh;
			}
			else
			{
				mesh = meshFilter.sharedMesh;
			}
			if (mesh != null)
			{
				if (modelConfig.CheckVertices && mesh.vertexCount > modelConfig.VerticesCount)
				{
					Debug.LogError("资源管理系统拒绝文件\"" + modelConfig.AssetPath + "\"。因为物体\"" + t.name + "\" vertice count " + mesh.vertexCount + " > " + modelConfig.VerticesCount);
					go = false;
				}
				int tCount = mesh.triangles.Length / 3;
				if (go && modelConfig.CheckTriangles && tCount > modelConfig.TrianglesCount)
				{
					Debug.LogError("资源管理系统拒绝文件\"" + modelConfig.AssetPath + "\"。因为物体\"" + t.name + "\" triangles count " + tCount + " > " + modelConfig.TrianglesCount);
					go = false;
				}
			}
			if (go && !modelConfig.modelMeshConfig.ImportMaterial)
			{
				List<Renderer> rs = new List<Renderer>();
				Renderer tmpRender = t.GetComponent<Renderer>();
				if (tmpRender != null) rs.Add(tmpRender);
				for (int j = 0; j < rs.Count; j++)
				{
					tmpRender = rs[j];
					if (tmpRender != null)
					{
						Material[] mats = tmpRender.sharedMaterials;
						for (int i = 0; i < mats.Length; i++)
						{
							Material mat = mats[i];
							if (mat != null)
							{
								Shader shader = mat.shader;
								if (shader != null && shader.name.Length >= 8 && shader.name.Substring(0, 8).CompareTo("Standard") == 0)
								{
									mats[i] = null;
								}
							}
						}
						tmpRender.sharedMaterials = mats;
					}
				}
			}
			foreach (Transform i in t.transform)
			{
				if (go)
				{
					checkMesh(i, ref go);
				}
				else
				{
					break;
				}
			}
		}
		private void deleteFile(string path)
		{
			EditorApplication.delayCall = delegate
			{
				AssetDatabase.DeleteAsset(path);
			};
		}
		public void RecalculateNormals(Mesh mesh, float angle)
		{
			var cosineThreshold = Mathf.Cos(angle * Mathf.Deg2Rad);
			var vertices = mesh.vertices;
			var normals = new Vector3[vertices.Length];
			// Holds the normal of each triangle in each sub mesh.
			var triNormals = new Vector3[mesh.subMeshCount][];
			var dictionary = new Dictionary<VertexKey, List<VertexEntry>>(vertices.Length);

			for (var subMeshIndex = 0; subMeshIndex < mesh.subMeshCount; ++subMeshIndex)
			{
				var triangles = mesh.GetTriangles(subMeshIndex);
				triNormals[subMeshIndex] = new Vector3[triangles.Length / 3];

				for (var i = 0; i < triangles.Length; i += 3)
				{
					int i1 = triangles[i];
					int i2 = triangles[i + 1];
					int i3 = triangles[i + 2];
					// Calculate the normal of the triangle
					Vector3 p1 = vertices[i2] - vertices[i1];
					Vector3 p2 = vertices[i3] - vertices[i1];
					Vector3 normal = Vector3.Cross(p1, p2).normalized;
					int triIndex = i / 3;
					triNormals[subMeshIndex][triIndex] = normal;
					List<VertexEntry> entry;
					VertexKey key;
					if (!dictionary.TryGetValue(key = new VertexKey(vertices[i1]), out entry))
					{
						entry = new List<VertexEntry>(4);
						dictionary.Add(key, entry);
					}
					entry.Add(new VertexEntry(subMeshIndex, triIndex, i1));
					if (!dictionary.TryGetValue(key = new VertexKey(vertices[i2]), out entry))
					{
						entry = new List<VertexEntry>();
						dictionary.Add(key, entry);
					}
					entry.Add(new VertexEntry(subMeshIndex, triIndex, i2));
					if (!dictionary.TryGetValue(key = new VertexKey(vertices[i3]), out entry))
					{
						entry = new List<VertexEntry>();
						dictionary.Add(key, entry);
					}
					entry.Add(new VertexEntry(subMeshIndex, triIndex, i3));
				}
			}
			// Each entry in the dictionary represents a unique vertex position.

			foreach (var vertList in dictionary.Values)
			{

				for (var i = 0; i < vertList.Count; ++i)
				{
					var sum = new Vector3();
					var lhsEntry = vertList[i];

					for (var j = 0; j < vertList.Count; ++j)
					{
						var rhsEntry = vertList[j];
						if (lhsEntry.VertexIndex == rhsEntry.VertexIndex)
						{
							sum += triNormals[rhsEntry.MeshIndex][rhsEntry.TriangleIndex];
						}
						else
						{
							// The dot product is the cosine of the angle between the two triangles.
							// A larger cosine means a smaller angle.
							var dot = Vector3.Dot(
							triNormals[lhsEntry.MeshIndex][lhsEntry.TriangleIndex],
							triNormals[rhsEntry.MeshIndex][rhsEntry.TriangleIndex]);
							if (dot >= cosineThreshold)
							{
								sum += triNormals[rhsEntry.MeshIndex][rhsEntry.TriangleIndex];
							}
						}
					}
					normals[lhsEntry.VertexIndex] = sum.normalized;
				}
			}
			mesh.normals = normals;
		}
		private struct VertexKey
		{
			private readonly long _x;
			private readonly long _y;
			private readonly long _z;
			// Change this if you require a different precision.
			private const int Tolerance = 100000;
			// Magic FNV values. Do not change these.
			private const long FNV32Init = 0x811c9dc5;
			private const long FNV32Prime = 0x01000193;

			public VertexKey(Vector3 position)
			{
				_x = (long)(Mathf.Round(position.x * Tolerance));
				_y = (long)(Mathf.Round(position.y * Tolerance));
				_z = (long)(Mathf.Round(position.z * Tolerance));
			}

			public override bool Equals(object obj)
			{
				var key = (VertexKey)obj;
				return _x == key._x && _y == key._y && _z == key._z;
			}

			public override int GetHashCode()
			{
				long rv = FNV32Init;
				rv ^= _x;
				rv *= FNV32Prime;
				rv ^= _y;
				rv *= FNV32Prime;
				rv ^= _z;
				rv *= FNV32Prime;
				return rv.GetHashCode();
			}
		}
		private struct VertexEntry
		{
			public int MeshIndex;
			public int TriangleIndex;
			public int VertexIndex;

			public VertexEntry(int meshIndex, int triIndex, int vertIndex)
			{
				MeshIndex = meshIndex;
				TriangleIndex = triIndex;
				VertexIndex = vertIndex;
			}
		}
	}
}
