﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using System.IO;
using CenturyGame.AssetSettings;


//[CreateAssetMenu(fileName = "AssetSettingConfigData", menuName = "CenturyGame/AssetSetting/AssetSettingConfigData")]
public class AssetSettingConfigData : ScriptableObject
{
	public Object currentConfigFile;
	public bool autoSave = true;
}

public class AssetSettingManager
{
	private static AssetSettingManager instance = null;

	//public string plugRootPath;//插件在Asset内的目录
	//public string configPath { get { return plugRootPath + "Config/"; } }//保存配置文件的文件夹目录
	public string GetCurrentConfigFilePath
	{
		get
		{
			if (currentConfigFile)
				return AssetDatabase.GetAssetPath(currentConfigFile);
			else
				return "";
		}
	}//当前引用的具体配置文件的路径
	private static string jsonpath = $"{Application.dataPath}/../Packages/";
	private static string jsonname = "assetsetting.json";

	private Object currentConfigFile;
	private bool autoSave = true;
	public string currentConfigFilePath;

	public Object CurrentConfigFile
	{
		get
		{
			if (!currentConfigFile)//如果没有配置文件
			{
				if (currentConfigFilePath.EndsWith(".dat"))
				{
					//Debug.Log("加载配置文件" + currentConfigFilePath);
					if (File.Exists(currentConfigFilePath))
					{
						currentConfigFile = AssetDatabase.LoadAssetAtPath(currentConfigFilePath, typeof(Object));
					}
				}
			}
			return currentConfigFile;
		}
		set
		{
			currentConfigFile = value;
			currentConfigFilePath = GetCurrentConfigFilePath;
			SaveToJson();
		}
	}

	static private string autoSavePrefsID = $"AssetSettingAutoSave_{PlayerSettings.productName}";

	public bool AutoSave
	{
		get => autoSave;
		set
		{
			if (value != autoSave)
			{
				autoSave = value;
				PlayerPrefs.SetInt(autoSavePrefsID, value ? 1 : 0);
				//SaveToJson();
			}
		}
	}

	private static void SaveToJson()
	{
		if (!Directory.Exists(jsonpath))//如果没有Packages文件夹则创建
		{
			Directory.CreateDirectory(jsonpath);
		}
		string jd = JsonUtility.ToJson(instance, true);
		File.WriteAllBytes(jsonpath + jsonname, System.Text.Encoding.UTF8.GetBytes(jd));
		Debug.Log($"{jsonname}配置文件已更改");
	}

	public static AssetSettingManager Instance
	{
		get
		{
			Setup();
			return instance;
		}
		set
		{
			Setup();
			instance = value;
		}
	}

	/// <summary>
	/// 兼容读取老版本的配置文件
	/// </summary>
	private static AssetSettingManager GetOldAssetData()
	{
		string[] guids = AssetDatabase.FindAssets("AssetSettingConfigData");//如果项目里存在AssetSettingConfigData，那么一定是老项目，内部有AssetSetting插件，所以可以使用FindAssets获取

		string dataPath = "";

		foreach (var _guid in guids)//验证是以".asset"结尾的，以免搜到同名脚本cs文件
		{
			dataPath = AssetDatabase.GUIDToAssetPath(_guid);
			if (dataPath.EndsWith(".asset"))
			{
				break;
			}
			else
			{
				dataPath = "";
			}
		}

		if (guids.Length == 0 || dataPath == "")//如果没有找到data文件
		{
			return null;
		}
		else
		{
			AssetSettingConfigData ascd = (AssetSettingConfigData)AssetDatabase.LoadAssetAtPath(dataPath, typeof(AssetSettingConfigData));//载入Data文件
			AssetSettingManager asm = new AssetSettingManager();
			asm.AutoSave = ascd.autoSave;
			asm.CurrentConfigFile = ascd.currentConfigFile;
			//Debug.Log("name:" + ascd.currentConfigFile);
			AssetDatabase.DeleteAsset(dataPath);//读出来然后删除旧资源
			return asm;
		}

	}

	private static void Setup()
	{
		if (instance == null)
		{
			if (!File.Exists(jsonpath + jsonname))//如果没有json
			{
				Debug.LogWarning("项目内没有找到任何符合要求的assetsetting.json配置文件，自动创建配置文件");

				instance = GetOldAssetData();//为了兼容老的配置，先看是否有旧配置项
				if (instance == null)
				{
					instance = new AssetSettingManager();// ScriptableObject.CreateInstance<AssetSettingConfigData>();
				}
				else
				{
					Debug.Log("继承老版本AssetSettingConfigData资源");
				}
				SaveToJson();
			}

			byte[] bt = File.ReadAllBytes(jsonpath + jsonname);
			instance = JsonUtility.FromJson<AssetSettingManager>(System.Text.Encoding.UTF8.GetString(bt));


			instance.autoSave = PlayerPrefs.GetInt(autoSavePrefsID, 1) == 1;



			/*
			if (guids.Length == 0 || dataPath == "")//如果没有找到data文件
			{
				Debug.LogWarning("项目内没有找到任何符合要求的AssetSettingConfigData配置文件，重新创建配置文件目录");
				string plugLocalDataPath = "/CenturyGamePackageRes/AssetSetting/Data/";
				string appDataPath = Application.dataPath + plugLocalDataPath;
				if (!Directory.Exists(appDataPath))
				{
					Directory.CreateDirectory(appDataPath);
				}

				instance = ScriptableObject.CreateInstance<AssetSettingConfigData>();
				dataPath = "Assets" + plugLocalDataPath + "AssetSettingConfigData.asset";
				AssetDatabase.CreateAsset(instance, dataPath);
				Debug.Log("创建AssetSettingConfigData,在" + dataPath);

				string plugLocalCfgPath = "/CenturyGamePackageRes/AssetSetting/Config/";
				string appCfgPath = Application.dataPath + plugLocalCfgPath;//dat配置文件路径

				if (!Directory.Exists(appCfgPath))
				{
					Directory.CreateDirectory(appCfgPath);
				}
				AssetSetting.SaveConfig("Assets" + plugLocalCfgPath + "ImportSetting.dat");

				AssetDatabase.SaveAssets();
				AssetDatabase.Refresh();
			}
			else
			{

			}

			if (instance)
			{
				//instance.plugRootPath = dataPath.Replace("Data/AssetSettingConfigData.asset", "");//设置初始的配置文件目录
				if (instance.currentConfigFile == null)
				{
					//string[] cfgPaths = AssetDatabase.FindAssets("ImportSetting", new string[] { instance.configPath });//寻找叫做ImportSetting的配置文件
					string _datPath = instance.configPath + "ImportSetting.dat";
					if (File.Exists(_datPath))
					{
						instance.currentConfigFile = AssetDatabase.LoadAssetAtPath(_datPath, typeof(Object));
						Debug.Log("加载默认配置：" + instance.currentConfigFilePath);
					}
					else
					{
						Debug.LogWarning("配置目录" + instance.configPath + "中没有找到任何dat配置文件");//或“请使用按钮创建”
					}
				}
			}
			else
			{
				Debug.LogError("AssetSettingConfigData实例创建失败");
			}
			*/
		}
	}
}
