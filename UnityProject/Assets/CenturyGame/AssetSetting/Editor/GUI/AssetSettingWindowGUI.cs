﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using System.IO;

namespace CenturyGame.AssetSettings
{
	/// <summary>
	/// AssetSetting功能的WindowGUI
	/// </summary>
	public class AssetSettingWindowGUI : EditorWindow
	{
		public static readonly int WindowWidth = 1300;
		public static readonly int WindowHeight = 950;
		private static AssetSettingWindowGUI assetSettingWindow;
		public static PageGUIBase SettingGUI = null;

		public static void ShowWindow()
		{
			Init();
		}

		public static float Width
		{
			get
			{
				return assetSettingWindow.position.width;
			}
		}
		public static float Height
		{
			get
			{
				return assetSettingWindow.position.height;
			}
		}

		/// <summary>
		/// 初始化窗口位置
		/// </summary>
		public static void Init()
		{
			if (assetSettingWindow)
			{
				return;
			}
			assetSettingWindow = (AssetSettingWindowGUI)EditorWindow.GetWindow(typeof(AssetSettingWindowGUI));
			assetSettingWindow.titleContent = new GUIContent("M5资源管理系统V2.1");
			//assetSettingWindow.position = new Rect((Screen.currentResolution.width - WindowWidth) / 2, (Screen.currentResolution.height - WindowHeight) / 2, WindowWidth, WindowHeight);
			AssetSetting.GetConfig();
			SettingGUI = new PageGUIBase(AssetSetting.Setting);

			assetSettingWindow.Show();
		}

		void OnEnable()
		{
			Init();
		}

		void OnDisable()
		{
			assetSettingWindow = null;
		}

		void OnDestroy()
		{
			if (AssetSettingManager.Instance.AutoSave && AssetSetting.Setting != null)
			{
				AssetSetting.SaveConfig();
			}
		}

		void Update()
		{
			//Init();
		}

		public Object currentConfigFile
		{
			get
			{
				return AssetSettingManager.Instance.CurrentConfigFile;
			}
			set
			{
				if (AssetSettingManager.Instance.CurrentConfigFile != value)
				{
					AssetSettingManager.Instance.CurrentConfigFile = value;
					//EditorUtility.SetDirty(AssetSettingConfigData.Instance);
					//AssetDatabase.SaveAssets();
				}
			}
		}


		public static Event GUIevent = null;
		//这里开始描绘UI--------------------------------↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓
		void OnGUI()
		{
			if (!assetSettingWindow)
				return;

			GUIevent = Event.current;
			Color oriColor = GUI.color;
			if (AssetSetting.Setting == null) return;

			EditorGUILayout.Space();
			EditorGUILayout.BeginHorizontal(GUILayout.Height(25));
			EditorGUILayout.Space();
			currentConfigFile = EditorGUILayout.ObjectField("资源管理配置文件(*.dat)", currentConfigFile, typeof(Object), false, GUILayout.Width(280));

			if (currentConfigFile)
			{

				if (GUILayout.Button("读取配置", GUILayout.Width(80)))
				{
					AssetSetting.ReloadConfig();
				}

				GUI.color = new Color(0.5f, 0.8f, 1f);
				if (GUILayout.Button("保存配置", GUILayout.Width(80)))
				{
					AssetSetting.SaveConfig();
				}
				GUI.color = oriColor;

			}
			else
			{
				GUI.color = new Color(0.5f, 0.8f, 1f);
				if (GUILayout.Button("创建/重载配置", GUILayout.Width(180)))
				{
					AssetSetting.CreatConfig();
				}
				GUI.color = oriColor;
			}

			EditorGUILayout.LabelField("", GUILayout.Width(AssetSettingWindowGUI.Width - 860));

			GUI.color = new Color(1f, 0.8f, 0.8f);
			AssetSetting.DisableInLocal = EditorGUILayout.Toggle("只在本机项目禁用资源管理", AssetSetting.DisableInLocal);
			GUI.color = oriColor;

			EditorGUILayout.LabelField("", GUILayout.Width(20));
			AssetSettingManager.Instance.AutoSave = EditorGUILayout.Toggle("关闭窗口时自动保存", AssetSettingManager.Instance.AutoSave);

			EditorGUILayout.EndHorizontal();
			string[] m_ButtonStr = new string[] { "图片设置", "模型设置", "其他设置" };
			//标签栏
			AssetSetting.Setting.SelectedPage = GUILayout.Toolbar(AssetSetting.Setting.SelectedPage, m_ButtonStr, GUILayout.Height(30));

			switch (AssetSetting.Setting.SelectedPage)
			{
				case 0:
					{
						SettingGUI.Texture.Draw();
						break;
					}
				case 1:
					{
						SettingGUI.Model.Draw();
						break;
					}
				case 2:
					{
						SettingGUI.Other.Draw();
						break;
					}
			}
		}



		void OnInspectorUpdate()
		{
			this.Repaint();
		}
	}
}