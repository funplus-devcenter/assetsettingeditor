﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using System.IO;
using System.Text;

namespace CenturyGame.AssetSettings
{
	public sealed class OtherGUI : IAssetSettingDraw
	{

		public OtherGUI(OtherImportSetting setting)
		{
			Setting = setting;
		}
		public OtherImportSetting Setting = null;

		public void Draw()
		{
			EditorGUILayout.BeginHorizontal(GUILayout.Height(40));
			EditorGUILayout.LabelField("", GUILayout.Width(95));
			EditorGUILayout.EndHorizontal();
			EditorGUILayout.BeginHorizontal(GUILayout.Height(40));
			Setting.FileNameAllowChinese = GUILayout.Toggle(Setting.FileNameAllowChinese, "容许资源包含中文名", GUILayout.Width(160));
			EditorGUILayout.LabelField("资源名称包含中文名触发操作：", GUILayout.Width(150));
			bool delete = Setting.DeleteFileNameHasChinese;
			delete = GUILayout.Toggle(delete, "报警并删除", GUILayout.Width(80));
			delete = GUILayout.Toggle(!delete, "报警", GUILayout.Width(50));
			Setting.DeleteFileNameHasChinese = !delete;
			EditorGUILayout.EndHorizontal();
			EditorGUILayout.BeginHorizontal(GUILayout.Height(25));
			EditorGUILayout.LabelField("白名单文件夹:");
			EditorGUILayout.EndHorizontal();

			for (int i = 0; i < Setting.WhiteList.Count; i++)
			{
				FolderEditorString path = Setting.WhiteList[i];
				EditorGUILayout.BeginHorizontal();
				AssetSetting.stringBuilder.Length = 0;
				AssetSetting.stringBuilder.Append(i + 1);
				AssetSetting.stringBuilder.Append(":");
				path.Select = GUILayout.Toggle(path.Select, AssetSetting.stringBuilder.ToString(), GUILayout.Width(40));
				path.Path = GUILayout.TextField(path.Path, GUILayout.Width(AssetSettingWindowGUI.Width - 220));
				if (path.Save)
				{
					AssetSetting.stringBuilder.Length = 0;
					AssetSetting.stringBuilder.Append("启用");
					path.Enable = GUILayout.Toggle(path.Enable, AssetSetting.stringBuilder.ToString(), GUILayout.Width(80));
				}
				else
				{
					if (GUILayout.Button("保存目录", GUILayout.Width(80)))
					{
						if ((!string.IsNullOrEmpty(path.Path)) && Directory.Exists(path.Path))
						{
							path.Save = true;
							path.Enable = true;
						}
						else
						{
							EditorUtility.DisplayDialog("图片设置", "目录无效，请重新输入！", "确定");
						}
					}
				}
				EditorGUILayout.EndHorizontal();
			}
			EditorGUILayout.BeginHorizontal(GUILayout.Height(25));
			EditorGUILayout.LabelField("", GUILayout.Width(AssetSettingWindowGUI.Width - 165));
			if (GUILayout.Button("+", GUILayout.Width(40)))
			{
				Setting.AddPath("", false, false);
			}
			if (GUILayout.Button("-", GUILayout.Width(40)))
			{

				for (int i = 0; i < Setting.WhiteList.Count; i++)
				{
					FolderEditorString path = Setting.WhiteList[i];
					if (path.Select)
					{
						Setting.WhiteList.RemoveAt(i);
						i--;
					}
				}
			}
			EditorGUILayout.EndHorizontal();
			PageGUIBase.DragPath(Setting.AddPath);
		}
	}
}