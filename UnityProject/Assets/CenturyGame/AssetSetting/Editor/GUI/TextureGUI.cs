﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using System.IO;
using System.Text;
namespace CenturyGame.AssetSettings
{
	/// <summary>
	/// 贴图资源导入设置标签类
	/// </summary>
	public sealed class TextureGUI : IAssetSettingDraw
	{
		public TextureGUI(TextureImportSetting setting)
		{
			Setting = setting;
		}
		public TextureImportSetting Setting = null;
		private int moveId = -1;
		//1为向上;2为向下
		private byte moveOp = 1;

		private Rect currentPathRect;

		Color winColor;
		Color uiRed = new Color(1f, 0.95f, 0.9f);
		Color uiGreen = new Color(0.7f, 1f, 0.8f);
		/// <summary>
		/// 该标签的GUI描绘----------------------------------------------------------------------------------------
		/// </summary>
		public void Draw()
		{
			winColor = GUI.color;
			EditorGUILayout.BeginHorizontal(GUILayout.Height(20));
			Setting.Enable = GUILayout.Toggle(Setting.Enable, "启用图片设置", GUILayout.Width(100));


			/*
			if (GUILayout.Button("图片检查", GUILayout.Width(80)))
			{
				EditorApplication.ExecuteMenuItem("Window/Texture Overview");
			}
			*/

			EditorGUILayout.EndHorizontal();

			EditorGUILayout.LabelField("白名单词缀(使用','隔开，当路径包含下方词缀时不使用AssetSetting处理)");

			Setting.whiteList = GUILayout.TextField(Setting.whiteList);

			EditorGUILayout.LabelField("提示：选择包含透明通道的压缩方式时，没有透明通道的图片资源仍会自动匹配不带透明的压缩方式。");

			EditorGUI.BeginDisabledGroup(!Setting.Enable || AssetSetting.DisableInLocal);

			EditorGUILayout.BeginVertical(EditorStyles.helpBox);
			EditorGUILayout.BeginHorizontal();
			EditorGUILayout.LabelField("默认压缩策略：");
			EditorGUILayout.EndHorizontal();
			drawCompression(Setting.DefaultCompressionConfig);
			//DrawImporSettings(Setting.DefaultConfig);//默认策略组
			EditorGUILayout.EndVertical();

			if (GUILayout.Button("[+] 添加组", GUILayout.Width(100)))
			{
				Setting.TextureGroupSelect = Setting.Groups.Count;
				TextureGroupItem item = new TextureGroupItem("未命名" + Setting.Groups.Count);
				item.AddPath("Assets/import");
				item.AddSuffix();
				item.Select = true;
				Setting.Groups.Add(item);
			}

			for (int i = 0; i < Setting.Groups.Count; i++)//循环描绘每一个贴图导入设置规则组-----------------------------------------
			{
				TextureGroupItem group = Setting.Groups[i];
				drawGroup(group, i);
			}
			EditorGUI.EndDisabledGroup();

			if (moveId >= 0)
			{
				TextureGroupItem group = Setting.Groups[moveId];
				if (moveOp == 1)
				{
					if (moveId >= 1)
					{
						Setting.Groups[moveId] = Setting.Groups[moveId - 1];
						Setting.Groups[moveId - 1] = group;
						Setting.TextureGroupSelect = moveId - 1;
					}
				}
				else
				{
					if (moveId <= Setting.Groups.Count - 2)
					{
						Setting.Groups[moveId] = Setting.Groups[moveId + 1];
						Setting.Groups[moveId + 1] = group;
						Setting.TextureGroupSelect = moveId + 1;
					}
				}
				moveId = -1;
			}
		}

		/// <summary>
		/// 描绘贴图设置的一个规则组
		/// </summary>
		/// <param name="item"></param>
		/// <param name="id"></param>
		void drawGroup(TextureGroupItem item, int id)
		{
			if (Setting.TextureGroupSelect == id)//如果当前是选中的组
			{
				EditorGUILayout.BeginHorizontal(EditorStyles.helpBox);//不限高
			}
			else
			{
				EditorGUILayout.BeginHorizontal(EditorStyles.helpBox, GUILayout.Height(25));//限高
			}

			EditorGUILayout.LabelField("第" + (id + 1).ToString() + "组", GUILayout.Width(40));

			//item.Select = GUILayout.Toggle(item.Select, AssetSetting.stringBuilder.ToString(), GUILayout.Width(50));将选中状态框Toggle隐藏
			item.Config.scroll = EditorGUILayout.BeginScrollView(item.Config.scroll, false, false, GUILayout.Width(AssetSettingWindowGUI.Width - 52));

			EditorGUILayout.BeginHorizontal(GUILayout.Height(25), GUILayout.Width(AssetSettingWindowGUI.Width - 100));
			EditorGUILayout.LabelField("组名称:", GUILayout.Width(90));
			item.Name = GUILayout.TextField(item.Name, GUILayout.Width(160));
			item.Enable = GUILayout.Toggle(item.Enable, "启用", GUILayout.Width(50));

			if (Setting.TextureGroupSelect != id)
			{
				EditorGUILayout.EndHorizontal();
				item.Select = false;
				EditorGUILayout.EndScrollView();
				EditorGUILayout.EndHorizontal();
				var rect = GUILayoutUtility.GetLastRect();
				var e = Event.current;
				if (item.Select || (e.type == EventType.MouseDown && rect.Contains(e.mousePosition)))
				{
					Setting.TextureGroupSelect = id;
				}
				return;//如果没有被选择，则不会继续描绘-------------------------------------------------------------------------------------没有被选择 从这里切断
			}
			else
			{
				item.Select = true;
				if (GUILayout.Button("↑", GUILayout.Width(40)))
				{
					moveId = id;
					moveOp = 1;
				}
				if (GUILayout.Button("↓", GUILayout.Width(40)))
				{
					moveId = id;
					moveOp = 2;
				}

				GUI.color = uiRed;
				if (GUILayout.Button("删除组", GUILayout.Width(80)))
				{

					if (item.Select)
					{
						Setting.Groups.RemoveAt(id);
					}
					if (Setting.TextureGroupSelect > 0)
					{
						Setting.Groups[Setting.TextureGroupSelect - 1].Select = true;
					}
					else if (Setting.TextureGroupSelect == 0)
					{
						if (Setting.Groups.Count > 0)
						{
							Setting.TextureGroupSelect = Setting.Groups.Count - 1;
							Setting.Groups[Setting.TextureGroupSelect].Select = true;
						}
					}
				}
				GUI.color = winColor;
				EditorGUILayout.EndHorizontal();
			}

			DrawImporSettings(item.Config);//路径策略设置组
			currentPathRect = EditorGUILayout.BeginVertical();//EditorStyles.helpBox
			GUILayout.Box("", GUILayout.Width(AssetSettingWindowGUI.Width - 150), GUILayout.Height(1));

			if (PageGUIBase.DragPath(Setting.AddPath, currentPathRect))//检测拖动路径+++++++++++++++++++++++++++++++++++
			{
				//原本想给描绘一个悬停提示框，但是每帧两次GUI描绘中，第二次描绘始终检测不到鼠标悬停，所以无法描绘。以后再说吧
			}


			EditorGUILayout.BeginHorizontal();
			//EditorGUI.DrawRect(new Rect(0, 0, AssetSettingWindowGUI.Width - 100, 1), Color.black);
			EditorGUILayout.LabelField("组策略文件夹");//组策略文件夹---下面是路径条-----------------------------------------------------------------
			EditorGUILayout.EndHorizontal();

			for (int i = 0; i < item.Folders.Count; i++)
			{
				FolderEditorString path = item.Folders[i];
				EditorGUILayout.BeginHorizontal();
				GUI.color = uiRed;
				if (GUILayout.Button("X", GUILayout.Width(20), GUILayout.Height(18)))//删除条目按钮
				{
					item.Folders.RemoveAt(i);
				}
				GUI.color = winColor;
				//path.Select = GUILayout.Toggle(path.Select, AssetSetting.stringBuilder.ToString(), GUILayout.Width(40));选择框暂时禁用
				EditorGUILayout.LabelField((i + 1).ToString() + ":", GUILayout.Width(20));//标题编号
				path.Path = GUILayout.TextField(path.Path, GUILayout.Width(AssetSettingWindowGUI.Width - 280));//路径文本框
				if (path.Save)
				{
					if (GUILayout.Button("重新导入", GUILayout.Width(80), GUILayout.Height(16)))
					{
						Debug.Log("刷新路径贴图资源：" + path.Path);
						AssetSetting.ImportAssets(path.Path, typeof(Texture2D));
					}
				}
				else
				{
					if (GUILayout.Button("保存目录", GUILayout.Width(80), GUILayout.Height(16)))
					{
						if ((!string.IsNullOrEmpty(path.Path)) && Directory.Exists(path.Path))
						{
							path.Save = true;
						}
						else
						{
							EditorUtility.DisplayDialog("图片设置", "目录无效，请重新输入！", "确定");
						}
					}
				}

				EditorGUILayout.EndHorizontal();
			}
			EditorGUILayout.Space();
			EditorGUILayout.BeginHorizontal(GUILayout.Height(25));
			if (GUILayout.Button("+ 添加路径匹配规则", GUILayout.Width(120)))
			{
				item.AddPath("Empty path/...", false);
			}
			EditorGUILayout.LabelField("或将文件夹拖动到此处添加", GUILayout.Width(AssetSettingWindowGUI.Width - 200));

			/* 原来的选中条目删除按钮，暂时停用，改为直接点击条目按钮删除
			if (GUILayout.Button("-", GUILayout.Width(40)))
			{
				for (int i = 0; i < item.Folders.Count; i++)
				{
					FolderEditorString path = item.Folders[i];
					if (path.Select)
					{
						item.Folders.RemoveAt(i);
						i--;
					}
				}
			}
			*/
			EditorGUILayout.EndHorizontal();

			EditorGUILayout.EndVertical();

			GUILayout.Box("", GUILayout.Width(AssetSettingWindowGUI.Width - 150), GUILayout.Height(1));

			EditorGUILayout.BeginHorizontal(GUILayout.Height(25));
			EditorGUILayout.LabelField("特殊文件名匹配:");
			EditorGUILayout.EndHorizontal();

			for (int i = 0; i < item.SpecialSuffixs.Count; i++)//描绘每个特殊配置导入设置组
			{
				TextureSpecialSuffix special = item.SpecialSuffixs[i];

				EditorGUILayout.BeginHorizontal(GUILayout.Width(50));
				GUI.color = uiRed;
				if (GUILayout.Button("X", GUILayout.Width(20), GUILayout.Height(18)))
				{
					item.SpecialSuffixs.RemoveAt(i);
				}
				GUI.color = winColor;

				AssetSetting.stringBuilder.Length = 0;
				AssetSetting.stringBuilder.Append(i + 1);
				AssetSetting.stringBuilder.Append(":");

				//special.Select = GUILayout.Toggle(special.Select, AssetSetting.stringBuilder.ToString(), GUILayout.Width(40));

				EditorGUILayout.LabelField(AssetSetting.stringBuilder.ToString(), GUILayout.Width(20));//标题编号
				drawSpecialConfig(special);//词缀匹配设置组？

				EditorGUILayout.EndHorizontal();
			}


			EditorGUILayout.BeginHorizontal(GUILayout.Height(25));
			//EditorGUILayout.LabelField("", GUILayout.Width(AssetSettingWindowGUI.Width - 185));
			if (GUILayout.Button("+ 添加词缀匹配规则", GUILayout.Width(120)))
			{
				item.AddSuffix();
			}
			/*删除按钮移到每个条目中
			if (GUILayout.Button("-", GUILayout.Width(40)))
			{
				for (int i = 0; i < item.SpecialSuffixs.Count; i++)
				{
					TextureSpecialSuffix special = item.SpecialSuffixs[i];
					if (special.Select)
					{
						item.SpecialSuffixs.RemoveAt(i);
						i--;
					}
				}
			}
			*/
			EditorGUILayout.EndHorizontal();
			EditorGUILayout.EndScrollView();
			EditorGUILayout.EndHorizontal();
		}



		/// <summary>
		/// 描绘路径匹配设置组
		/// </summary>
		void DrawImporSettings(TextureGroupConfig conf)
		{
			EditorGUILayout.BeginVertical(EditorStyles.helpBox);
			/*
			EditorGUILayout.BeginHorizontal();
			EditorGUILayout.LabelField("资源模板贴图", GUILayout.Width(90));
			conf.texTemplate = (Texture)EditorGUILayout.ObjectField(conf.texTemplate, typeof(Object), false, GUILayout.Width(160));

			if (GUILayout.Button("从模板贴图中获取设置", GUILayout.Width(120)))
			{
				conf.ConfigFromTexTemplate();
			}

			if (GUILayout.Button("复制配置", GUILayout.Width(80)))
			{
				AssetSettingConfigData.Instance.lastCopyTextureConfig = conf.ToJson();
				Debug.Log("复制了一个贴图导入配置信息.");
			}

			if (GUILayout.Button("粘贴配置", GUILayout.Width(80)))
			{
				string s = AssetSettingConfigData.Instance.lastCopyTextureConfig;
				if (s == "")
				{
					Debug.Log("没有找到任何配置信息，请先进行复制");
				}
				else
				{
					conf.CopyFrom(TextureGroupConfig.FromJson(s));
				}
			}

			EditorGUILayout.EndHorizontal();
			EditorGUILayout.Space();
			*/

			TextureImporterConfig importConf = conf.textureImporterConfig;
			importConf.Enable = GUILayout.Toggle(importConf.Enable, "修改贴图导入设置", GUILayout.Width(120));
			if (importConf.Enable)
			{
				EditorGUILayout.Space();
				drawImportConf(importConf);
			}



			conf.textureCompressionConfig.Enable = GUILayout.Toggle(conf.textureCompressionConfig.Enable, "修改贴图压缩设置", GUILayout.Width(120));
			if (conf.textureCompressionConfig.Enable)
			{
				EditorGUILayout.Space();
				drawCompression(conf.textureCompressionConfig);
			}

			/*
			setCompressionQuality(conf.textureCompressionConfig.SettingPc);
			setCompressionQuality(conf.textureCompressionConfig.SettingAndroid);
			setCompressionQuality(conf.textureCompressionConfig.SettingIos);
			*/
			EditorGUILayout.EndVertical();
		}

		/// <summary>
		/// 描绘贴图导入设置部分的UI
		/// </summary>
		void drawImportConf(TextureImporterConfig importConf)
		{
			EditorGUILayout.BeginHorizontal();
			EditorGUILayout.LabelField("Texture Type:", GUILayout.Width(90));
			importConf.TextureType = (TextureImporterType)EditorGUILayout.EnumPopup("", importConf.TextureType, GUILayout.Width(60));
			EditorGUILayout.LabelField("Texture Shape:", GUILayout.Width(95));
			importConf.TextureShape = (TextureImporterShape)EditorGUILayout.EnumPopup("", importConf.TextureShape, GUILayout.Width(80));
			importConf.RWEnable = GUILayout.Toggle(importConf.RWEnable, "R/W Enable", GUILayout.Width(90));
			importConf.SRGB = GUILayout.Toggle(importConf.SRGB, "SRGB", GUILayout.Width(60));
			EditorGUILayout.LabelField("NPOT Scale:", GUILayout.Width(80));
			importConf.NPOTScale = (TextureImporterNPOTScale)EditorGUILayout.EnumPopup("", importConf.NPOTScale, GUILayout.Width(70));
			importConf.SMipMap = GUILayout.Toggle(importConf.SMipMap, "SMipMap", GUILayout.Width(70));
			importConf.MipMap = GUILayout.Toggle(importConf.MipMap, "MipMap", GUILayout.Width(60));
			EditorGUILayout.EndHorizontal();
			EditorGUILayout.BeginHorizontal();
			EditorGUILayout.LabelField("Max Size:", GUILayout.Width(60));
			importConf.MaxSize = (TextureSize)EditorGUILayout.EnumPopup("", importConf.MaxSize, GUILayout.Width(70));
			importConf.ignoreSmaller = GUILayout.Toggle(importConf.ignoreSmaller, "Ignore Smaller", GUILayout.Width(120));

			EditorGUILayout.LabelField("Lock From FileSize:", GUILayout.Width(120));
			importConf.lockFromFileSize = (FoldedRate)EditorGUILayout.EnumPopup("", importConf.lockFromFileSize, GUILayout.Width(70));
			EditorGUILayout.LabelField("Alpha Source:", GUILayout.Width(90));
			importConf.AlphaSource = (TextureImporterAlphaSource)EditorGUILayout.EnumPopup("", importConf.AlphaSource, GUILayout.Width(70));
			importConf.AlphaIsTransparency = GUILayout.Toggle(importConf.AlphaIsTransparency, "Transparency", GUILayout.Width(100));

			EditorGUILayout.EndHorizontal();
			EditorGUILayout.BeginHorizontal(GUILayout.Height(25));

			EditorGUILayout.LabelField("Warp Mode:", GUILayout.Width(80));
			importConf.WrapMod = (TextureWrapMode)EditorGUILayout.EnumPopup("", importConf.WrapMod, GUILayout.Width(70));
			EditorGUILayout.LabelField("Filter Mode:", GUILayout.Width(80));
			importConf.FilterMode = (FilterMode)EditorGUILayout.EnumPopup("", importConf.FilterMode, GUILayout.Width(70));
			/*不管理AnisoLevel，美术应按照每张贴图的栅格化程度自行设置
			EditorGUILayout.LabelField("Aniso Level:", GUILayout.Width(80));
			conf.AnisoLevel = EditorGUILayout.IntSlider(conf.AnisoLevel, 0, 16, GUILayout.Width(180));
			*/
			EditorGUILayout.EndHorizontal();
		}


		/// <summary>
		/// 描绘压缩设置部分UI 三相之力
		/// </summary>
		/// <param name="_textureCompressionConfig"></param>
		void drawCompression(TextureCompressionConfig _textureCompressionConfig)
		{
			EditorGUILayout.BeginHorizontal(GUILayout.Height(25));
			_textureCompressionConfig.SettingPc.overridden = GUILayout.Toggle(_textureCompressionConfig.SettingPc.overridden, "Standalone", GUILayout.Width(85));
			EditorGUILayout.LabelField("Resize Algorothm:", GUILayout.Width(110));
			_textureCompressionConfig.SettingPc.resizeAlgorithm = (TextureResizeAlgorithm)EditorGUILayout.EnumPopup("", _textureCompressionConfig.SettingPc.resizeAlgorithm, GUILayout.Width(70));
			EditorGUILayout.LabelField("Format:", GUILayout.Width(50));
			_textureCompressionConfig.SettingPc.format = (TextureImporterFormat)EditorGUILayout.EnumPopup("", _textureCompressionConfig.SettingPc.format, GUILayout.Width(170));
			//EditorGUILayout.LabelField("Compressor Quality:", GUILayout.Width(120));
			//_textureCompressionConfig.SettingPc.textureCompression = (TextureImporterCompression)EditorGUILayout.EnumPopup("", _textureCompressionConfig.SettingPc.textureCompression, GUILayout.Width(100));
			EditorGUILayout.EndHorizontal();
			EditorGUILayout.BeginHorizontal(GUILayout.Height(25));
			_textureCompressionConfig.SettingAndroid.overridden = GUILayout.Toggle(_textureCompressionConfig.SettingAndroid.overridden, "Android", GUILayout.Width(70));
			EditorGUILayout.LabelField("Resize Algorothm:", GUILayout.Width(110));
			_textureCompressionConfig.SettingAndroid.resizeAlgorithm = (TextureResizeAlgorithm)EditorGUILayout.EnumPopup("", _textureCompressionConfig.SettingAndroid.resizeAlgorithm, GUILayout.Width(70));
			EditorGUILayout.LabelField("Format:", GUILayout.Width(50));
			_textureCompressionConfig.SettingAndroid.format = (TextureImporterFormat)EditorGUILayout.EnumPopup("", _textureCompressionConfig.SettingAndroid.format, GUILayout.Width(170));
			/*
			EditorGUILayout.LabelField("Compressor Quality:", GUILayout.Width(120));
			_textureCompressionConfig.SettingAndroid.textureCompression = (TextureImporterCompression)EditorGUILayout.EnumPopup("", _textureCompressionConfig.SettingAndroid.textureCompression, GUILayout.Width(100));
			EditorGUILayout.LabelField("Fallback:", GUILayout.Width(55));
			_textureCompressionConfig.SettingAndroid.androidETC2FallbackOverride = (AndroidETC2FallbackOverride)EditorGUILayout.EnumPopup("", _textureCompressionConfig.SettingAndroid.androidETC2FallbackOverride, GUILayout.Width(70));
			*/
			EditorGUILayout.EndHorizontal();
			EditorGUILayout.BeginHorizontal(GUILayout.Height(25));
			_textureCompressionConfig.SettingIos.overridden = GUILayout.Toggle(_textureCompressionConfig.SettingIos.overridden, "iPhone", GUILayout.Width(55));
			EditorGUILayout.LabelField("Resize Algorothm:", GUILayout.Width(110));
			_textureCompressionConfig.SettingIos.resizeAlgorithm = (TextureResizeAlgorithm)EditorGUILayout.EnumPopup("", _textureCompressionConfig.SettingIos.resizeAlgorithm, GUILayout.Width(70));
			EditorGUILayout.LabelField("Format:", GUILayout.Width(50));
			_textureCompressionConfig.SettingIos.format = (TextureImporterFormat)EditorGUILayout.EnumPopup("", _textureCompressionConfig.SettingIos.format, GUILayout.Width(170));
			//EditorGUILayout.LabelField("Compressor Quality:", GUILayout.Width(120));
			//_textureCompressionConfig.SettingIos.textureCompression = (TextureImporterCompression)EditorGUILayout.EnumPopup("", _textureCompressionConfig.SettingIos.textureCompression, GUILayout.Width(100));
			EditorGUILayout.EndHorizontal();
		}

		/// <summary>
		/// 描绘前后缀匹配规则的设置组
		/// </summary>
		void drawSpecialConfig(TextureSpecialSuffix special)
		{
			EditorGUILayout.BeginVertical();

			EditorGUILayout.BeginHorizontal(GUILayout.Width(200));
			special.Match = (MatchMod)EditorGUILayout.EnumPopup("", special.Match, GUILayout.Width(100));//前中后缀枚举选择
			special.Suffix = GUILayout.TextField(special.Suffix, GUILayout.Width(100));//填写词缀名称
			EditorGUILayout.EndHorizontal();

			EditorGUILayout.Space();//前缀title结束 向下留空位

			DrawImporSettings(special);

			EditorGUILayout.EndVertical();
		}

		void setCompressionQuality(TextureImporterPlatformSettings s)
		{

			switch (s.textureCompression)
			{
				case TextureImporterCompression.Uncompressed:
					{
						s.compressionQuality = 100;
						break;
					}
				case TextureImporterCompression.Compressed:
					{
						s.compressionQuality = 0;
						break;
					}
				case TextureImporterCompression.CompressedLQ:
					{
						s.compressionQuality = 50;
						break;
					}
				case TextureImporterCompression.CompressedHQ:
					{
						s.compressionQuality = 100;
						break;
					}
			}
		}
	}
}