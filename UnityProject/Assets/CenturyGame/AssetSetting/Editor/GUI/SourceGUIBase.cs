﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using System.IO;
using System.Text;

namespace CenturyGame.AssetSettings
{

	public class SourceGUIBase : IAssetSettingDraw
	{
		public ModelImportSetting Setting = null;
		protected private int moveId = -1;
		//1为向上;2为向下
		protected private byte moveOp = 1;
		private Rect currentPathRect;

		Color winColor;
		Color uiRed = new Color(1f, 0.95f, 0.9f);
		Color uiGreen = new Color(0.7f, 1f, 0.8f);

		public virtual void Draw()
		{
			DrawHead();

			if (GUILayout.Button("增加组", GUILayout.Width(80)))
			{
				Setting.TextureGroupSelect = Setting.Groups.Count;
				ModelGroupItem item = new ModelGroupItem("未命名" + Setting.Groups.Count);
				item.AddPath("Assets/import");
				item.AddSuffix();
				item.Select = true;
				Setting.Groups.Add(item);
			}

			for (int i = 0; i < Setting.Groups.Count; i++)
			{
				ModelGroupItem group = Setting.Groups[i];
				//drawGroup(group, i);
			}
			EditorGUI.EndDisabledGroup();
			PageGUIBase.DragPath(Setting.AddPath);

			if (moveId >= 0)
			{
				ModelGroupItem group = Setting.Groups[moveId];
				if (moveOp == 1)
				{
					if (moveId >= 1)
					{
						Setting.Groups[moveId] = Setting.Groups[moveId - 1];
						Setting.Groups[moveId - 1] = group;
						Setting.TextureGroupSelect = moveId - 1;
					}
				}
				else
				{
					if (moveId <= Setting.Groups.Count - 2)
					{
						Setting.Groups[moveId] = Setting.Groups[moveId + 1];
						Setting.Groups[moveId + 1] = group;
						Setting.TextureGroupSelect = moveId + 1;
					}
				}
				moveId = -1;
			}
		}

		public virtual void DrawHead()
		{
			EditorGUILayout.BeginHorizontal(GUILayout.Height(40));
			Setting.Enable = GUILayout.Toggle(Setting.Enable, "启用设置", GUILayout.Width(100));

			EditorGUILayout.EndHorizontal();
			EditorGUILayout.LabelField("提示：");

			EditorGUI.BeginDisabledGroup(!Setting.Enable || AssetSetting.DisableInLocal);
			EditorGUILayout.BeginVertical(EditorStyles.helpBox);
			EditorGUILayout.BeginHorizontal();
			EditorGUILayout.LabelField("默认策略：");
			EditorGUILayout.EndHorizontal();
			//drawGroupConfig(Setting.DefaultConfig);
			EditorGUILayout.EndVertical();
		}
	}
}