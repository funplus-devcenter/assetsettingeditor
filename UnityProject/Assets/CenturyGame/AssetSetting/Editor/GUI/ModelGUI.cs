﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using System.IO;
using System.Text;

namespace CenturyGame.AssetSettings
{

	public sealed class ModelGUI : IAssetSettingDraw
	{
		public ModelGUI(ModelImportSetting setting)
		{
			Setting = setting;
		}
		public ModelImportSetting Setting = null;
		private int moveId = -1;
		//1为向上;2为向下
		private byte moveOp = 1;

		private Rect currentPathRect;

		Color winColor;
		Color uiRed = new Color(1f, 0.95f, 0.9f);
		Color uiGreen = new Color(0.7f, 1f, 0.8f);

		public void Draw()
		{
			winColor = GUI.color;
			EditorGUILayout.BeginHorizontal(GUILayout.Height(30));
			Setting.Enable = GUILayout.Toggle(Setting.Enable, "启用模型设置", GUILayout.Width(100));
			if (GUILayout.Button("增加组", GUILayout.Width(80)))
			{
				Setting.TextureGroupSelect = Setting.Groups.Count;
				ModelGroupItem item = new ModelGroupItem("未命名" + Setting.Groups.Count);
				item.AddPath("Assets/import");
				item.AddSuffix();
				item.Select = true;
				Setting.Groups.Add(item);
			}

			EditorGUILayout.EndHorizontal();
			EditorGUILayout.LabelField("提示：");

			EditorGUI.BeginDisabledGroup(!Setting.Enable || AssetSetting.DisableInLocal);

			/*
			EditorGUILayout.BeginVertical(EditorStyles.helpBox);
			EditorGUILayout.BeginHorizontal();
			EditorGUILayout.LabelField("默认策略：");
			EditorGUILayout.EndHorizontal();
			DrawImporSettings(Setting.DefaultConfig);
			EditorGUILayout.EndVertical();
			*/

			for (int i = 0; i < Setting.Groups.Count; i++)
			{
				ModelGroupItem group = Setting.Groups[i];
				drawGroup(group, i);
			}
			EditorGUI.EndDisabledGroup();

			if (moveId >= 0)
			{
				ModelGroupItem group = Setting.Groups[moveId];
				if (moveOp == 1)
				{
					if (moveId >= 1)
					{
						Setting.Groups[moveId] = Setting.Groups[moveId - 1];
						Setting.Groups[moveId - 1] = group;
						Setting.TextureGroupSelect = moveId - 1;
					}
				}
				else
				{
					if (moveId <= Setting.Groups.Count - 2)
					{
						Setting.Groups[moveId] = Setting.Groups[moveId + 1];
						Setting.Groups[moveId + 1] = group;
						Setting.TextureGroupSelect = moveId + 1;
					}
				}
				moveId = -1;
			}
		}

		void drawGroup(ModelGroupItem item, int id)
		{
			if (Setting.TextureGroupSelect == id)//如果当前是选中的组
			{
				EditorGUILayout.BeginHorizontal(EditorStyles.helpBox);//不限高
			}
			else
			{
				EditorGUILayout.BeginHorizontal(EditorStyles.helpBox, GUILayout.Height(25));//限高
			}
			EditorGUILayout.LabelField("第" + (id + 1).ToString() + "组", GUILayout.Width(40));

			//item.Select = GUILayout.Toggle(item.Select, AssetSetting.stringBuilder.ToString(), GUILayout.Width(50));
			item.Config.scroll = EditorGUILayout.BeginScrollView(item.Config.scroll, false, false);
			EditorGUILayout.BeginHorizontal(GUILayout.Height(25));
			EditorGUILayout.LabelField("组名称:", GUILayout.Width(90));
			item.Name = GUILayout.TextField(item.Name, GUILayout.Width(160));
			item.Enable = GUILayout.Toggle(item.Enable, "启用", GUILayout.Width(50));
			if (Setting.TextureGroupSelect != id)
			{
				EditorGUILayout.EndHorizontal();
				item.Select = false;
				EditorGUILayout.EndScrollView();
				EditorGUILayout.EndHorizontal();
				var rect = GUILayoutUtility.GetLastRect();
				var e = Event.current;
				if (item.Select || (e.type == EventType.MouseDown && rect.Contains(e.mousePosition)))
				{
					Setting.TextureGroupSelect = id;
				}
				return;//如果没有被选择，则不会继续描绘-------------------------------------------------------------------------------------没有被选择 从这里切断
			}
			else
			{
				item.Select = true;
				if (GUILayout.Button("↑", GUILayout.Width(40)))
				{
					moveId = id;
					moveOp = 1;
				}
				if (GUILayout.Button("↓", GUILayout.Width(40)))
				{
					moveId = id;
					moveOp = 2;
				}

				GUI.color = uiRed;
				if (GUILayout.Button("删除组", GUILayout.Width(80)))
				{

					if (item.Select)
					{
						Setting.Groups.RemoveAt(id);
					}
					if (Setting.TextureGroupSelect > 0)
					{
						Setting.Groups[Setting.TextureGroupSelect - 1].Select = true;
					}
					else if (Setting.TextureGroupSelect == 0)
					{
						if (Setting.Groups.Count > 0)
						{
							Setting.TextureGroupSelect = Setting.Groups.Count - 1;
							Setting.Groups[Setting.TextureGroupSelect].Select = true;
						}
					}
				}
				GUI.color = winColor;
				EditorGUILayout.EndHorizontal();
			}

			DrawImporSettings(item.Config);

			currentPathRect = EditorGUILayout.BeginVertical();//EditorStyles.helpBox
			GUILayout.Box("", GUILayout.Width(AssetSettingWindowGUI.Width - 150), GUILayout.Height(1));

			if (PageGUIBase.DragPath(Setting.AddPath, currentPathRect))//检测拖动路径+++++++++++++++++++++++++++++++++++
			{
				//原本想给描绘一个悬停提示框，但是每帧两次GUI描绘中，第二次描绘始终检测不到鼠标悬停，所以无法描绘。以后再说吧
			}

			EditorGUILayout.BeginHorizontal();
			EditorGUILayout.LabelField("组策略文件夹:");
			EditorGUILayout.EndHorizontal();

			for (int i = 0; i < item.Folders.Count; i++)
			{
				ModelFolderEditorString path = item.Folders[i];
				EditorGUILayout.BeginHorizontal();
				GUI.color = uiRed;
				if (GUILayout.Button("X", GUILayout.Width(20), GUILayout.Height(18)))//删除条目按钮
				{
					item.Folders.RemoveAt(i);
				}
				GUI.color = winColor;
				//path.Select = GUILayout.Toggle(path.Select, AssetSetting.stringBuilder.ToString(), GUILayout.Width(40));
				EditorGUILayout.LabelField((i + 1).ToString() + ":", GUILayout.Width(20));//标题编号
				path.Path = GUILayout.TextField(path.Path, GUILayout.Width(AssetSettingWindowGUI.Width - 640));
				EditorGUILayout.LabelField("", GUILayout.Width(20));
				path.CheckVertices = GUILayout.Toggle(path.CheckVertices, "顶点约束:", GUILayout.Width(65));
				path.VerticesCount = EditorGUILayout.IntField(path.VerticesCount, GUILayout.Width(60));
				path.CheckTriangles = GUILayout.Toggle(path.CheckTriangles, "面数约束:", GUILayout.Width(65));
				path.TrianglesCount = EditorGUILayout.IntField(path.TrianglesCount, GUILayout.Width(60));
				if (path.Save)
				{
					if (GUILayout.Button("重新导入", GUILayout.Width(80)))
					{
						AssetSetting.ImportAssets(path.Path, typeof(GameObject));
					}
				}
				else
				{
					if (GUILayout.Button("保存目录", GUILayout.Width(80)))
					{
						if ((!string.IsNullOrEmpty(path.Path)) && Directory.Exists(path.Path))
						{
							path.Save = true;
						}
						else
						{
							EditorUtility.DisplayDialog("图片设置", "目录无效，请重新输入！", "确定");
						}
					}
				}
				EditorGUILayout.EndHorizontal();
			}
			EditorGUILayout.Space();
			EditorGUILayout.BeginHorizontal(GUILayout.Height(25));

			if (GUILayout.Button("+ 添加路径匹配规则", GUILayout.Width(120)))
			{
				item.AddPath("Empty path/...", false);
			}

			EditorGUILayout.LabelField("或将文件夹拖动到此处添加", GUILayout.Width(AssetSettingWindowGUI.Width - 200));


			/* 原来的选中条目删除按钮，暂时停用，改为直接点击条目按钮删除
			if (GUILayout.Button("+", GUILayout.Width(40)))
			{
				item.AddPath("", false);
			}
			if (GUILayout.Button("-", GUILayout.Width(40)))
			{

				for (int i = 0; i < item.Folders.Count; i++)
				{
					FolderEditorString path = item.Folders[i];
					if (path.Select)
					{
						item.Folders.RemoveAt(i);
						i--;
					}
				}
			}
			*/
			EditorGUILayout.EndHorizontal();

			EditorGUILayout.EndVertical();
			GUILayout.Box("", GUILayout.Width(AssetSettingWindowGUI.Width - 150), GUILayout.Height(1));

			EditorGUILayout.BeginHorizontal(GUILayout.Height(25));
			EditorGUILayout.LabelField("特殊文件名匹配:");
			EditorGUILayout.EndHorizontal();

			for (int i = 0; i < item.SpecialSuffixs.Count; i++)
			{
				ModelSpecialSuffix special = item.SpecialSuffixs[i];
				EditorGUILayout.BeginHorizontal(GUILayout.Width(50));


				GUI.color = uiRed;
				if (GUILayout.Button("X", GUILayout.Width(20), GUILayout.Height(18)))
				{
					item.SpecialSuffixs.RemoveAt(i);
				}
				GUI.color = winColor;

				AssetSetting.stringBuilder.Length = 0;
				AssetSetting.stringBuilder.Append(i + 1);
				AssetSetting.stringBuilder.Append(":");

				EditorGUILayout.LabelField(AssetSetting.stringBuilder.ToString(), GUILayout.Width(20));//标题编号

				drawSpecialConfig(special);

				EditorGUILayout.EndHorizontal();
			}
			EditorGUILayout.BeginHorizontal(GUILayout.Height(25));

			if (GUILayout.Button("+ 添加词缀匹配规则", GUILayout.Width(120)))
			{
				item.AddSuffix();
			}

			EditorGUILayout.EndHorizontal();
			EditorGUILayout.EndScrollView();
			EditorGUILayout.EndHorizontal();
		}
		void DrawImporSettings(ModelGroupConfig conf)
		{
			EditorGUILayout.BeginVertical(EditorStyles.helpBox);

			ModelMeshConfig importConf = conf.modelMeshConfig;
			importConf.Enable = GUILayout.Toggle(importConf.Enable, "修改模型导入设置", GUILayout.Width(120));
			if (importConf.Enable)
			{
				EditorGUILayout.Space();
				drawImportConf(conf.modelMeshConfig);
			}

			ModelAnimationConfig animConf = conf.modelAnimationConfig;
			animConf.Enable = GUILayout.Toggle(animConf.Enable, "修改动画导入设置", GUILayout.Width(120));
			if (animConf.Enable)
			{
				EditorGUILayout.Space();
				drawAnimationConf(conf.modelAnimationConfig);
			}

			EditorGUILayout.EndVertical();
		}

		void drawSpecialConfig(ModelSpecialSuffix special)
		{
			EditorGUILayout.BeginVertical();

			EditorGUILayout.BeginHorizontal(GUILayout.Width(240));

			special.Match = (MatchMod)EditorGUILayout.EnumPopup("", special.Match, GUILayout.Width(80));
			special.Suffix = GUILayout.TextField(special.Suffix, GUILayout.Width(100));
			special.CheckVertices = GUILayout.Toggle(special.CheckVertices, "顶点约束:", GUILayout.Width(65));
			special.VerticesCount = EditorGUILayout.IntField(special.VerticesCount, GUILayout.Width(60));
			special.CheckTriangles = GUILayout.Toggle(special.CheckTriangles, "面数约束:", GUILayout.Width(65));
			special.TrianglesCount = EditorGUILayout.IntField(special.TrianglesCount, GUILayout.Width(60));
			EditorGUILayout.EndHorizontal();

			DrawImporSettings(special);
			EditorGUILayout.EndVertical();
		}

		/// <summary>
		/// 描绘模型导入设置部分的UI
		/// </summary>
		void drawImportConf(ModelMeshConfig importConf)
		{
			EditorGUILayout.BeginHorizontal(GUILayout.Height(25));
			EditorGUILayout.LabelField("Scale Factor:", GUILayout.Width(80));
			importConf.ScaleFactor = EditorGUILayout.FloatField(importConf.ScaleFactor, GUILayout.Width(30));
			importConf.UseFileScale = GUILayout.Toggle(importConf.UseFileScale, "File Scale", GUILayout.Width(80));
			EditorGUILayout.LabelField("Compression:", GUILayout.Width(85));
			importConf.MeshCompression = (ModelImporterMeshCompression)EditorGUILayout.EnumPopup(importConf.MeshCompression, GUILayout.Width(70));
			importConf.RW = GUILayout.Toggle(importConf.RW, "R/W", GUILayout.Width(60));
			importConf.OptimizeMesh = GUILayout.Toggle(importConf.OptimizeMesh, "Optimize Mesh", GUILayout.Width(110));
			importConf.ImportBlendShape = GUILayout.Toggle(importConf.ImportBlendShape, "Import BlendShapes", GUILayout.Width(130));
			importConf.GenerateCollider = GUILayout.Toggle(importConf.GenerateCollider, "Generate Colliders", GUILayout.Width(130));
			importConf.KeepQuad = GUILayout.Toggle(importConf.KeepQuad, "Keep Quads", GUILayout.Width(110));
			EditorGUILayout.LabelField("Index Format:", GUILayout.Width(85));
			importConf.IndexFormat = (ModelImporterIndexFormat)EditorGUILayout.EnumPopup(importConf.IndexFormat, GUILayout.Width(60));
			EditorGUILayout.EndHorizontal();

			EditorGUILayout.BeginHorizontal(GUILayout.Height(25));
			importConf.ImportMaterial = GUILayout.Toggle(importConf.ImportMaterial, "Import Material", GUILayout.Width(120));
			importConf.WeldVertice = GUILayout.Toggle(importConf.WeldVertice, "Weld Vertices", GUILayout.Width(110));
			importConf.ImportVisibility = GUILayout.Toggle(importConf.ImportVisibility, "Import Visibility", GUILayout.Width(120));
			importConf.ImportCamera = GUILayout.Toggle(importConf.ImportCamera, "Import Cameras", GUILayout.Width(120));
			importConf.ImportLight = GUILayout.Toggle(importConf.ImportLight, "Import Lights", GUILayout.Width(110));
			importConf.PreserveHierarchy = GUILayout.Toggle(importConf.PreserveHierarchy, "Preserve Hierarchy", GUILayout.Width(130));
			importConf.SwapUV = GUILayout.Toggle(importConf.SwapUV, "Swap UVs", GUILayout.Width(80));
			//importConf.GenerateLightmapUV = GUILayout.Toggle(importConf.GenerateLightmapUV, "Generate Lightmap UVs", GUILayout.Width(160));
			EditorGUILayout.EndHorizontal();

			EditorGUILayout.BeginHorizontal(GUILayout.Height(25));
			EditorGUILayout.LabelField("Normals:", GUILayout.Width(65));
			importConf.ImportNormal = (ModelImporterNormals)EditorGUILayout.EnumPopup(importConf.ImportNormal, GUILayout.Width(70));
			EditorGUILayout.LabelField("Normal Mode:", GUILayout.Width(80));
			importConf.NormalMode = (ModelImporterNormalCalculationMode)EditorGUILayout.EnumPopup(importConf.NormalMode, GUILayout.Width(90));
			EditorGUILayout.LabelField("Smoothing Angle:", GUILayout.Width(90));
			importConf.SmoothingAngle = EditorGUILayout.IntSlider(importConf.SmoothingAngle, 0, 180, GUILayout.Width(160));
			EditorGUILayout.LabelField("Tangents:", GUILayout.Width(80));
			importConf.Tangent = (ModelImporterTangents)EditorGUILayout.EnumPopup(importConf.Tangent, GUILayout.Width(120));
			EditorGUILayout.EndHorizontal();
		}

		void drawAnimationConf(ModelAnimationConfig animConf)
		{
			EditorGUILayout.BeginHorizontal();

			EditorGUILayout.LabelField("Anim Compression:", GUILayout.Width(90));
			animConf.animationCompression = (ModelImporterAnimationCompression)EditorGUILayout.EnumPopup(animConf.animationCompression, GUILayout.Width(150));

			animConf.OptimizeAniScale = GUILayout.Toggle(animConf.OptimizeAniScale, "Optimize Ani Scale", GUILayout.Width(140));
			animConf.OptimizeAniFloat = GUILayout.Toggle(animConf.OptimizeAniFloat, "Optimize Ani Float", GUILayout.Width(140));
			EditorGUILayout.LabelField("posError:", GUILayout.Width(50));
			animConf.posError = EditorGUILayout.FloatField(animConf.posError, GUILayout.Width(50));
			EditorGUILayout.LabelField("rotError:", GUILayout.Width(50));
			animConf.rotError = EditorGUILayout.FloatField(animConf.rotError, GUILayout.Width(50));
			EditorGUILayout.LabelField("sclError:", GUILayout.Width(50));
			animConf.sclError = EditorGUILayout.FloatField(animConf.sclError, GUILayout.Width(50));

			EditorGUILayout.EndHorizontal();
		}
	}
}
