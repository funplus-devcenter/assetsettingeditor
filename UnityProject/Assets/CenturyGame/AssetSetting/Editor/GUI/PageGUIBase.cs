﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using System.IO;
using System.Text;
using System.Text.RegularExpressions;

namespace CenturyGame.AssetSettings
{
	public class PageGUIBase
	{
		public IAssetSettingDraw Other;
		public IAssetSettingDraw Texture;
		public IAssetSettingDraw Model;

		public PageGUIBase(AssetSettingStore Setting)
		{
			Other = new OtherGUI(Setting.OtherSetting);
			Texture = new TextureGUI(Setting.TextureSetting);
			Model = new ModelGUI(Setting.ModelSetting);
		}

		/// <summary>
		/// 不传入Rect则默认整个窗口
		/// </summary>
		public static bool DragPath(System.Action<string> callback)
		{
			return DragPath(callback, new Rect(0, 0, AssetSettingWindowGUI.Width, AssetSettingWindowGUI.Height));
		}

		/// <summary>
		/// 传入Rect只识别响应Rect范围，返回布尔是否在Rect范围内
		/// </summary>
		public static bool DragPath(System.Action<string> callback, Rect rect)
		{
			bool inRect = false;
			//Rect rect = new Rect(0, 0, AssetSettingWindowGUI.Width, AssetSettingWindowGUI.Height);
			if ((Event.current.type == EventType.DragUpdated//拖放操作已更新

			|| Event.current.type == EventType.DragPerform)//或者执行拖放操作

			&& rect.Contains(Event.current.mousePosition))//事件发生在Rect区域
			{
				inRect = true;
				DragAndDrop.visualMode = DragAndDropVisualMode.Generic;

				if (Event.current.type == EventType.DragPerform && DragAndDrop.paths != null && DragAndDrop.paths.Length > 0)
				{
					string[] paths = DragAndDrop.paths;
					string path = string.Empty;

					for (int i = 0; i < paths.Length; i++)
					{
						path = paths[i];
						callback(path);
					}
				}
			}
			return inRect;
		}
	}

	/// <summary>
	/// 实现描绘接口，分别被TextureGUI、ModelGUI等实现使用
	/// </summary>
	public interface IAssetSettingDraw
	{
		void Draw();
	}
}