﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using System.IO;
using System.Text;

namespace CenturyGame.AssetSettings
{
	public class G2LAlphaCalWindow : EditorWindow
	{
		private static G2LAlphaCalWindow window;
		int numStr = 0;
		string resultByte = "0", resultFloat = "0";

		//[MenuItem("Tools/CenturyGame/透明度计算G2L")]
		static void Init()
		{
			window = (G2LAlphaCalWindow)EditorWindow.GetWindow(typeof(G2LAlphaCalWindow));
			window.titleContent = new GUIContent("G2L Alpha");
			window.position = new Rect((Screen.currentResolution.width - 400) / 2, (Screen.currentResolution.height - 300) / 2, 400, 300);
			window.Show();
		}

		void OnGUI()
		{
			numStr = EditorGUILayout.IntField("百分比(%):", numStr);
			if (numStr > 100) numStr = 100;
			if (numStr < 0) numStr = 0;
			if (GUILayout.Button("Cal"))
			{
				float f = numStr / 100.0f;
				f = Mathf.LinearToGammaSpace(f);
				resultByte = (255 * f).ToString("f0");
				resultFloat = f.ToString("f3");
			}
			EditorGUILayout.TextField("透明度数值[0-255]:", resultByte);
			EditorGUILayout.TextField("透明度数值[0-1]:", resultFloat);
		}

		//[MenuItem("Tools/CenturyGame/ChangeToAlpha8")]
		static void ChangeToAlpha8()
		{
			Texture2D target = Selection.activeObject as Texture2D;
			if (target != null)
			{
				Texture2D newTexture = new Texture2D(target.width, target.height, TextureFormat.Alpha8, false);
				Color[] cls = target.GetPixels();

				for (int i = 0; i < cls.Length; i++)
				{
					Color c = cls[i];
					c.a = c.r;
				}
				newTexture.SetPixels(cls);
				AssetDatabase.CreateAsset(newTexture, "Assets/BrushDraw/Brushes/" + target.name + ".asset");
			}
		}

		//[MenuItem("CenturyGame/AssetSetting/NormalRGB1Channel")]
		static void NormalRGB1Channel()
		{
			Texture2D target = Selection.activeObject as Texture2D;
			if (target != null)
			{
				Color[] cls = target.GetPixels();
				Texture2D t2d = new Texture2D(target.width, target.height, TextureFormat.RGBA32, true);
				Color color = Color.red;

				for (int i = 0; i < cls.Length; i++)
				{
					color = cls[i];
					color.a = color.r;
					color.r = color.b = color.g;
					cls[i] = color;
				}
				t2d.SetPixels(cls);
				t2d.Apply();
				System.IO.File.WriteAllBytes(@"C:\Project\Client\Client\M5\Assets\002\001_n4.png", t2d.EncodeToPNG());
			}
		}

		//[MenuItem("Tools/CenturyGame/NormalRGB12Channel")]
		static void NormalRGB12Channel()
		{
			Texture2D target = Selection.activeObject as Texture2D;
			if (target != null)
			{
				Color[] cls = target.GetPixels();
				Texture2D t2d = new Texture2D(target.width, target.height, TextureFormat.RGBA32, true);
				Color color = Color.red;

				for (int i = 0; i < cls.Length; i++)
				{
					color = cls[i];
					color.a = color.r * 1.2f - 0.1f;
					color.r = color.b = color.g * 1.2f - 0.1f;
					cls[i] = color;
				}
				t2d.SetPixels(cls);
				t2d.Apply();
				System.IO.File.WriteAllBytes(@"C:\Project\Client\Client\M5\Assets\002\001_n42.png", t2d.EncodeToPNG());
			}
		}

		//[MenuItem("Tools/CenturyGame/NormalRG1Channel")]
		static void NormalRG1Channel()
		{
			Texture2D target = Selection.activeObject as Texture2D;
			if (target != null)
			{
				Color[] cls = target.GetPixels();
				Texture2D t2d = new Texture2D(target.width, target.height, TextureFormat.RGBA32, true);
				Color color = Color.red;

				for (int i = 0; i < cls.Length; i++)
				{
					//c[i] = Gamma2Linear(c[i]);
					color = cls[i];
					color.a = color.r;
					color.r = color.g;
					cls[i] = color;
				}
				t2d.SetPixels(cls);
				t2d.Apply();
				System.IO.File.WriteAllBytes(@"C:\Project\Client\Client\M5\Assets\002\001_n5.png", t2d.EncodeToPNG());
			}
		}

		//[MenuItem("Tools/CenturyGame/NormalAddAlpha")]
		static void NormalAddAlpha()
		{
			Texture2D target = Selection.activeObject as Texture2D;
			if (target != null)
			{
				Color[] cls = target.GetPixels();
				Texture2D t2d = new Texture2D(target.width, target.height, TextureFormat.RGBA32, true);
				Color color = Color.red;

				for (int i = 0; i < cls.Length; i++)
				{
					//c[i] = Gamma2Linear(c[i]);
					color = cls[i];
					color.a = 0.5f;
					cls[i] = color;
				}
				t2d.SetPixels(cls);
				t2d.Apply();
				System.IO.File.WriteAllBytes(@"C:\Project\Client\Client\M5\Assets\002\001_n6.png", t2d.EncodeToPNG());
			}
		}

		//[MenuItem("Tools/CenturyGame/GetShaderNameFromFolder")]
		static void GetShaderFromFolder()
		{
			UnityEngine.Object o = Selection.activeObject;
			if (o == null) return;
			if (o.GetType() == typeof(UnityEditor.DefaultAsset))
			{
				string path = AssetDatabase.GetAssetPath(o);
				string currentPath = System.Environment.CurrentDirectory;
				int index = currentPath.Length + 1;
				currentPath = currentPath.Replace(Path.DirectorySeparatorChar, '/');
				currentPath = string.Concat(currentPath, "/", path);
				List<string> files = AssetSetting.GetFiles(currentPath, new string[] { ".prefab" }, index);
				Dictionary<string, Shader> shaderList = new Dictionary<string, Shader>();
				for (int i = 0; i < files.Count; i++)
				{
					GameObject obj = AssetDatabase.LoadAssetAtPath<GameObject>(files[i]);
					getRender(shaderList, obj.transform);
				}
				StringBuilder sb = new StringBuilder();
				foreach (KeyValuePair<string, Shader> item in shaderList)
				{
					sb.AppendLine(item.Key);
				}
				Debug.Log(sb.ToString());
			}
		}
		static void getRender(Dictionary<string, Shader> shaderList, Transform t)
		{
			Renderer r = t.GetComponent<Renderer>();
			if (r != null)
			{
				Material[] mats = r.sharedMaterials;
				for (int i = 0; i < mats.Length; i++)
				{
					Material mat = mats[i];
					if (mat != null)
					{
						if (!shaderList.ContainsKey(mat.shader.name))
						{
							shaderList[mat.shader.name] = mat.shader;
						}
					}
				}
			}
			foreach (Transform item in t)
			{
				getRender(shaderList, item);
			}
		}
	}
}
