using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using System;
using UnityEngine;
using System.Reflection;

public class AssetHelp
{
#if UNITY_EDITOR
	//[UnityEditor.MenuItem("Tools/CenturyGame/获取存储大小")]
	public static void GetSha1()
	{
		UnityEngine.Object o = UnityEditor.Selection.activeObject;
		//Debug.Log("手机内存:" + AbHelp.GetSizeStr(GetStorageMemory(o))  + ",编辑器内存:" + AbHelp.GetSizeStr(GetStorageMemory(o, true)));
	}
#endif
	
	/// <summary>
	/// 遍历组件
	/// </summary>
	public static void GetCom<T>(Transform root, List<T> list) where T : UnityEngine.Object
	{
		T com = root.GetComponent<T>();
		if (com != null)
		{
			list.Add(com);
		}
		foreach (Transform t in root)
		{
			GetCom<T>(t, list);
		}
	}

	public static long GetStorageMemory(UnityEngine.Object o, bool runtime = false)
	{
		long result = 0;
#if UNITY_EDITOR
		if (runtime)
		{
			result = UnityEngine.Profiling.Profiler.GetRuntimeMemorySizeLong(o);
		}
		else
		{
			if (o is AnimationClip)
			{
				result = (int)getAnimationMethod(o);
			}
			else if(o is Texture)
			{
				if (textureMethod == null)
				{
					textureMethod = getMethod("UnityEditor.TextureUtil, UnityEditor.dll", "GetStorageMemorySizeLong");
				}
				para[0] = o;
				result = (long)textureMethod.Execute(null, para);
			}
			else
			{
				result = UnityEngine.Profiling.Profiler.GetRuntimeMemorySizeLong(o);
			}
		}
#endif
		return result;
	}
#if UNITY_EDITOR
	private static DynamicMethodExecutor textureMethod;
	private static DynamicMethodExecutor getMethod(string className, string method)
	{
		var type = Type.GetType(className);
		System.Reflection.MethodInfo methodInfo = type.GetMethod (method
			, System.Reflection.BindingFlags.Static | System.Reflection.BindingFlags.Instance | System.Reflection.BindingFlags.Public);
		return new DynamicMethodExecutor(methodInfo);
	}


	private static object[] para = new object[]{null};
	private static FieldInfo aniSizeInfo;
	private static MethodInfo getAnimationClipStats;
	private static int getAnimationMethod(UnityEngine.Object o)
	{
//	        System.Object stats = null;
//	        if(animationMethod == null)
//	        {
//		        animationMethod = getMethod("UnityEditor.AnimationUtility, UnityEditor.dll", "GetAnimationClipStats");
//	        }
//	        para[0] = o;
//	        stats = animationMethod.Execute(null, para);
//	        if(aniSizeInfo == null)
//	        {
//		        Type aniclipstats = Type.GetType("UnityEditor.AnimationClipStats, UnityEditor.dll");
//		        aniSizeInfo = aniclipstats.GetField ("size" , System.Reflection.BindingFlags.Public | System.Reflection.BindingFlags.Instance);
//	        }
//	        return (int)aniSizeInfo.GetValue(stats);
		if (getAnimationClipStats == null)
		{
			getAnimationClipStats = typeof(UnityEditor.AnimationUtility).GetMethod("GetAnimationClipStats", BindingFlags.Static | BindingFlags.NonPublic);
		}
		
		if (aniSizeInfo == null)
		{
			Assembly asm = Assembly.GetAssembly(typeof(UnityEditor.Editor));
			Type aniclipstats = asm.GetType("UnityEditor.AnimationClipStats");
			aniSizeInfo = aniclipstats.GetField ("size", BindingFlags.Public | BindingFlags.Instance);
		}
		para[0] = o;
		var stats = getAnimationClipStats.Invoke(null, para);
		return (int)aniSizeInfo.GetValue(stats);
	}
	private static void clearGetStorageMemory()
	{
		textureMethod = null;
		aniSizeInfo = null;
		getAnimationClipStats = null;
		para = null;
	}
#endif
}